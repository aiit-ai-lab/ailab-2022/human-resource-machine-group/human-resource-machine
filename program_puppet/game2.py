import pygame

# 定义代码块的类
class CodeBlock(pygame.sprite.Sprite):
    def __init__(self, text, pos):
        super().__init__()
        self.image = pygame.Surface((100, 35))
        if text == "copyfrom 2":
            self.image.fill((196, 107, 87))
        elif text == "jump 4":
            self.image.fill((142, 139, 184))
        else:
            self.image.fill((156, 187, 93))
        self.rect = self.image.get_rect()
        self.rect.topleft = pos
        self.text = text
        self.is_dragging = False
        self.has_jump = False

    def update(self):
        font = pygame.font.Font("char.ttf", 18)
        if self.text == "copyfrom 2":
            text_color = (101, 33, 17)
            block_color = (196, 107, 87)
        elif self.text == "jump 4":
            text_color = (50, 54, 92)
            block_color = (142, 139, 184)
        else:
            text_color = (64, 87, 9)
            block_color = (156, 187, 93)
        self.image.fill(block_color)

        # 如果是空白代码块，不渲染文字
        if self.text != " ":
            text = font.render(self.text, False, text_color)
            self.image.blit(text, (5, 4))

pygame.init()
screen = pygame.display.set_mode((800, 600))
clock = pygame.time.Clock()
boss_image = pygame.image.load('boss.png')
boss_rect = boss_image.get_rect()
# 创建多个代码块
code_blocks = []
block = CodeBlock("inbox 1", (30, 40 * 1))
code_blocks.append(block)
block = CodeBlock("copyfrom 2", (30, 40 * 2))
code_blocks.append(block)
block = CodeBlock("outbox 3", (30, 40 * 3))
code_blocks.append(block)
block = CodeBlock("jump 4", (30, 40 * 4))
code_blocks.append(block)

dragging_block = None  # 当前被拖拽的代码块

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:  # 左键按下，尝试拖拽代码块
                for block in code_blocks:
                    if block.rect.collidepoint(event.pos):
                        dragging_block = block
                        offset_x = event.pos[0] - dragging_block.rect.x
                        offset_y = event.pos[1] - dragging_block.rect.y
                        break
        elif event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1 and dragging_block is not None:  # 左键释放，停止拖拽
                dragging_block.is_dragging = True
                if dragging_block.rect.right >= screen.get_width():  # 如果代码块拖到屏幕最右侧
                    sorted_blocks = sorted(code_blocks, key=lambda block: block.rect.top)
                    for i, block in enumerate(sorted_blocks):
                        print(f"代码块 {block.text} 的顺序是 {i + 1}")
                dragging_block = None

    if dragging_block is not None:  # 更新被拖拽代码块的位置
        dragging_block.rect.x = event.pos[0] - offset_x
        dragging_block.rect.y = event.pos[1] - offset_y

    mouse_x, mouse_y = pygame.mouse.get_pos()
    if boss_rect.collidepoint(mouse_x, mouse_y):
        # 显示绿色对话框
        # 可以通过绘制矩形来实现，如：
        pygame.draw.rect(screen, (0, 255, 0), (boss_rect.x, boss_rect.y - 20, 100, 20))
    # 绘制界面
    screen.fill((255, 255, 255))
    screen.blit(boss_image, boss_rect)
    for block in code_blocks:
        block.update()
        screen.blit(block.image, block.rect.topleft)

    pygame.display.flip()
    clock.tick(60)
