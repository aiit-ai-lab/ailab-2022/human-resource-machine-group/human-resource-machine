from figure import *


def start_game_logic(screen, background, character,play_button_image,hand_image,ground,cease,boss_image,next1,cease1,result):
    global sorted_blocks,sd,flag
    module_init(result)
    numbox(screen,result)

    while True:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            widgets(event,screen, play_button_image,cease,result)

            for block in code_blocks:
                block.handle_event(event, code_blocks,result)  # 处理鼠标事件
            for small_square in small_squares:
                small_square.handle_event(event)  # 处理小方块的鼠标事件
        # 绘制背景图片
        #screen.blit(cease, (188, 605))
        screen.blit(background, (0, 0))
        screen.blit(play_button_image, (320, 600))
        if not pd():
            screen.blit(cease, (210, 605))
        else:
            screen.blit(cease1, (210, 605))
        blocks.draw(screen)
        sign(screen, hand_image,boss_image,result)



        for block in code_blocks:
            block.upui()  # 更新代码块的显示
            screen.blit(block.image, block.rect.topleft)  # 绘制代码块
            if block.has_sub_block and block.linked_block is not None:
                calculation_thread = threading.Thread(target=calculate_curve_points, args=(screen,block,result,))
                calculation_thread.start()
                calculation_thread.join()


        for small_square in small_squares:
            small_square.update()  # 更新小方块的显示
            screen.blit(small_square.image, small_square.rect.topleft)

        character.update()
        character.control()
        character.draw(screen)
        screen.blit(ground, (887, 0))
        if result ==0:
            font = pygame.font.Font("title1.ttf", 36)
            text_render = font.render("负数的产生", True, (130, 105, 88))
            screen.blit(text_render, (930, 80))
        elif result ==1:
            font = pygame.font.Font("title1.ttf", 36)
            text_render = font.render("  勾股定理", True, (130, 105, 88))
            screen.blit(text_render, (930, 80))
        elif result ==2:
            font = pygame.font.Font("title1.ttf", 36)
            text_render = font.render("春秋 乘除法", True, (130, 105, 88))
            screen.blit(text_render, (930, 80))
        elif result ==3:
            font = pygame.font.Font("title1.ttf", 36)
            text_render = font.render("  等差数列", True, (130, 105, 88))
            screen.blit(text_render, (930, 80))
        elif result ==4:
            font = pygame.font.Font("title1.ttf", 36)
            text_render = font.render("球体积计算", True, (130, 105, 88))
            screen.blit(text_render, (930, 80))


        ending = False
        if result==0:
            if outbox == [-2,-2,-4]:
                ending = end_processing(character,screen,next1)
                screen.blit(next1, (400, 200))
        elif result==1:
            if len(outbox) == 1:
                ending = end_processing(character,screen,next1)
                screen.blit(next1, (400, 200))
        elif result==2:
            if outbox == [4,2,3]:
                ending = end_processing(character,screen,next1)
                screen.blit(next1, (400, 200))
        elif result==3:
            if outbox == [25,100]:
                ending = end_processing(character,screen,next1)
                screen.blit(next1, (400, 200))
        elif result==4:
            if outbox == [256,108,4,32]:
                ending = end_processing(character,screen,next1)
                screen.blit(next1, (400, 200))



        if ending == True:
            outbox.clear()
            code_blocks.clear()
            small_squares.clear()
            numbox(screen,result)
            blocks.empty()
            break
        pygame.display.flip()




def main(isin):
    global result
    # 初始化pygame
    pygame.init()
    try:

        # 设置游戏图标
        gameicon = pygame.image.load("BoxIcon.png")
        # # 展示游戏图标
        pygame.display.set_icon(gameicon)

        pygame.display.set_caption('I Love 古代数学')

        screen = pygame.display.set_mode((1200, 675))
        background = pygame.image.load("background5.jpg")  # 背景
        ground = pygame.image.load("back2.jpg")

        hand_image = pygame.image.load('hand.png')
        cease = pygame.image.load("cease.jpg")
        cease1 = pygame.image.load("cease1.jpg")
        next1 = pygame.image.load("next2.png")
        play_button_image = pygame.image.load("play_button.jpg")
        boss_image = pygame.image.load('boss.png')
        character = Character("char8.png", (300, 200))  # 人物
        # 主界面背景图片
        interface = pygame.image.load("begin.jpg")

        # “第一关”按钮
        button_1 = Button('zhoudown.png', 'zhouup.png', (556, 87))
        # “第二关”按钮
        button_2 = Button('qindown.png', 'qinup.png', (720, 185))
        # "第三关"按钮
        button_3 = Button('jindown.png', 'jinup.png', (475, 345))
        # "第四关"按钮
        button_4 = Button('nandown.png', 'nanup.png', (643, 515))
        # 主界面背景图片
        juanzhou1 = pygame.image.load("juanzhou1.png")
        juanzhou2 = pygame.image.load("juanzhou2.png")
        back_interface = pygame.image.load("backhong.png")
        interface = pygame.image.load("begin.png")

    except pygame.error as msg:
        raise (SystemExit(msg))
    a=0
    while True:
        if isin == 1:
            start_game_logic(screen, background, character, play_button_image, hand_image, ground, cease, boss_image,
                             next1,cease1, 0)
            break
        else:
            result = showGameInterface(screen, back_interface,interface, button_1, button_2,button_3,button_4,juanzhou1,juanzhou2,)
            if result == 0:  # 如果返回0，退出游戏
                break
            elif result == 1:  # 如果返回1，执行开始游戏的逻辑
                print('周')
                start_game_logic(screen, background, character,play_button_image,hand_image,ground,cease,boss_image,next1,cease1,1)
            elif result == 2:  # 如果返回2，执行游戏说明的逻辑
                print('秦')
                start_game_logic(screen, background, character, play_button_image, hand_image, ground, cease, boss_image,next1,cease1,2)
            elif result == 3:
                print('晋')
                start_game_logic(screen, background, character, play_button_image, hand_image, ground, cease, boss_image,next1,cease1,3)
            elif result == 4:
                print('南北')
                start_game_logic(screen, background, character, play_button_image, hand_image, ground, cease, boss_image,next1,cease1,4)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:  # 检查是否按下了空格键
                   a=1
        if a==1:
            break


if __name__ == '__main__':
    main(0)