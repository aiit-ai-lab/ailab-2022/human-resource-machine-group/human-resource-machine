import pygame
import sys

# 定义代码块的类
class CodeBlock(pygame.sprite.Sprite):
    def __init__(self, text, pos):
        super().__init__()
        self.image = pygame.Surface((100, 35))
        if text == "copyfrom 2":
            self.image.fill((196, 107, 87))
        else:
            self.image.fill((156, 187, 93))
        #self.image.fill((156,187,93))
        self.rect = self.image.get_rect()
        self.rect.topleft = pos
        self.text = text

    def update(self):
         #pygame.draw.rect(self.image, (0, 0, 0), self.rect)  # 绘制代码块框架
         #font = pygame.font.SysFont(None, 18)
         font = pygame.font.Font("char.ttf", 18)
         if self.text == "copyfrom 2":
            text = font.render(self.text, False, (101, 33, 17))
         else:
            text = font.render(self.text, False, (64, 87, 9))
         self.image.blit(text, (5, 4))

class Character(pygame.sprite.Sprite):
    def __init__(self, image, pos):
        super().__init__()
        self.image = pygame.image.load(image)
        self.rect = self.image.get_rect()
        #self.rect.topleft = (100, 100)
        self.rect.topleft = pos
        self.speed = 0.5

    def update(self, keys):
        # 根据按键状态更新角色位置
        if keys[pygame.K_UP]:
            self.rect.y -= self.speed  # 更新位置时乘以速度参数
        if keys[pygame.K_DOWN]:
            self.rect.y += self.speed  # 更新位置时乘以速度参数
        if keys[pygame.K_LEFT]:
            self.rect.x -= self.speed  # 更新位置时乘以速度参数
        if keys[pygame.K_RIGHT]:
            self.rect.x += self.speed
    def instruct(self,order):

        pass

# 初始化pygame
pygame.init()

# 设置窗口大小
screen_size = (1200, 675)
screen = pygame.display.set_mode(screen_size)
clock = pygame.time.Clock()
# 设置标题
pygame.display.set_caption("人物移动")
# 加载背景图片
background = pygame.image.load("background.jpg")
# 加载人物图片
character = Character("char.png", (100, 100))
#char_rect = char.get_rect()


# 创建多个代码块
code_blocks = []
#for i in range(3):
block = CodeBlock(f"inbox 1", (30, 40 * 1))
code_blocks.append(block)
block = CodeBlock(f"copyfrom 2", (30, 40 * 2))
code_blocks.append(block)
block = CodeBlock(f"outbox 3", (30, 40 * 3))
code_blocks.append(block)
dragging_block = None  # 当前被拖拽的代码块

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:  # 左键按下，尝试拖拽代码块
                for block in code_blocks:
                    if block.rect.collidepoint(event.pos):
                        dragging_block = block
                        offset_x = event.pos[0] - dragging_block.rect.x
                        offset_y = event.pos[1] - dragging_block.rect.y
                        break
        elif event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1 and dragging_block is not None:  # 左键释放，停止拖拽
                if dragging_block.rect.right >= screen.get_width():  # 如果代码块拖到屏幕最右侧
                    sorted_blocks = sorted(code_blocks, key=lambda block: block.rect.top)
                    character.instruct(sorted_blocks)
                    for i, block in enumerate(sorted_blocks):
                        print(f"代码块 {block.text} 的顺序是 {i + 1}")
                        # popped_element = block.pop()
                        # print("出栈元素：", popped_element)
                dragging_block = None

    # 获取按键状态
    keys = pygame.key.get_pressed()
    # 根据按键状态更新人物位置
    character.update(keys)
    # 绘制背景和人物 bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb
    screen.blit(background, (0, 0))
    screen.blit(character.image, character.rect.topleft)


    # 更新被拖拽代码块的位置  dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd
    if dragging_block is not None:
        dragging_block.rect.x = event.pos[0] - offset_x
        dragging_block.rect.y = event.pos[1] - offset_y

    # 绘制界面
    for block in code_blocks:
        block.update()
        screen.blit(block.image, block.rect.topleft)
    pygame.display.flip()
    clock.tick(60)

for i in range(1, 11):
    result = i ** 2 - ((i + 1) ** 2 - 4 * i)
    print(f"i={i}时，结果为：{result}")