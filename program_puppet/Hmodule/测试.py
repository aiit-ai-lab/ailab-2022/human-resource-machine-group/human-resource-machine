import pygame
import sys
import numpy as np
import math

pygame.init()

screen_width, screen_height = 800, 600
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("连接两个模块")

white = (255, 255, 255)
black = (0, 0, 0)

module_size = 50
modules = [pygame.Rect(100, 300, module_size, module_size),
           pygame.Rect(650, 300, module_size, module_size)]

button_font = pygame.font.Font(None, 36)
button_rect = pygame.Rect(50, 50, 200, 50)

selected_module = None
mouse_offset = (0, 0)

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            for module in modules:
                if module.collidepoint(event.pos):
                    selected_module = module
                    mouse_offset = (event.pos[0] - module.x, event.pos[1] - module.y)
            if button_rect.collidepoint(event.pos):
                # 生成一对新的相连模块
                new_module1 = pygame.Rect(300, 100, module_size, module_size)
                new_module2 = pygame.Rect(450, 100, module_size, module_size)
                modules.extend([new_module1, new_module2])

        elif event.type == pygame.MOUSEBUTTONUP:
            selected_module = None
        elif event.type == pygame.MOUSEMOTION:
            if selected_module:
                selected_module.x = event.pos[0] - mouse_offset[0]
                selected_module.y = event.pos[1] - mouse_offset[1]

    screen.fill(white)

    for module in modules:
        pygame.draw.rect(screen, black, module)

    pygame.draw.rect(screen, black, button_rect)
    button_text = button_font.render("生成模块", True, white)
    screen.blit(button_text, (65, 60))

    # 绘制连接线
    for i in range(0,len(modules)-1,2):
        module1 = modules[i]
        module2 = modules[i+1]
        control_point1 = (module1.x + int(math.sqrt(abs(module1.y - module2.y)*16)) * 0.5 + 60, (module1.y + module2.y + module_size) // 2)

        t = np.linspace(0, 1, 5000)
        x_curve = (1 - t) ** 2 * (module1.x + module_size) + 2 * (1 - t) * t * control_point1[0] + t ** 2 * (
                    module2.x + module_size)
        y_curve = (1 - t) ** 2 * (module1.y + module_size // 2) + 2 * (1 - t) * t * control_point1[1] + t ** 2 * (
                    module2.y + module_size // 2)

        curve_points = list(zip(x_curve, y_curve))
        pygame.draw.lines(screen, black, False, curve_points, 2)

    pygame.display.flip()

pygame.quit()
sys.exit()