import pygame
import sys

class CodeBlock(pygame.sprite.Sprite):
    def __init__(self, text, pos):
        super().__init__()
        self.image = pygame.Surface((100, 35))
        if text == "copyfrom 2":
            self.image.fill((196, 107, 87))
        else:
            self.image.fill((156, 187, 93))
        self.rect = self.image.get_rect()
        self.rect.topleft = pos
        self.text = text

    def update(self):
        font = pygame.font.Font("char.ttf", 18)
        if self.text == "copyfrom 2":
            text = font.render(self.text, False, (101, 33, 17))
        else:
            text = font.render(self.text, False, (64, 87, 9))
        self.image.blit(text, (5, 4))

class Game:
    def __init__(self):
        pygame.init()
        self.screen_size = (1200, 675)
        self.screen = pygame.display.set_mode(self.screen_size)
        self.clock = pygame.time.Clock()
        pygame.display.set_caption("人物移动")
        self.background = pygame.image.load("background.jpg")
        self.char = pygame.image.load("char.png")
        self.char_rect = self.char.get_rect()
        self.char_rect.topleft = (100, 100)
        self.code_blocks = []
        self.create_code_blocks()
        self.dragging_block = None

    def create_code_blocks(self):
        block = CodeBlock(f"inbox 1", (30, 40 * 1))
        self.code_blocks.append(block)
        block = CodeBlock(f"copyfrom 2", (30, 40 * 2))
        self.code_blocks.append(block)
        block = CodeBlock(f"outbox 3", (30, 40 * 3))
        self.code_blocks.append(block)

    def run(self):
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1: # 左键按下，尝试拖拽代码块
                        for block in self.code_blocks:
                            if block.rect.collidepoint(event.pos):
                                self.dragging_block = block
                                offset_x = event.pos[0] - self.dragging_block.rect.x
                                offset_y = event.pos[1] - self.dragging_block.rect.y
                                break
                elif event.type == pygame.MOUSEBUTTONUP:
                    if event.button == 1 and self.dragging_block is not None: # 左键释放，停止拖拽
                        if self.dragging_block.rect.right >= self.screen.get_width(): # 如果代码块拖到屏幕最右侧
                            sorted_blocks = sorted(self.code_blocks, key=lambda block: block.rect.top)
                            for i, block in enumerate(sorted_blocks):
                                print(f"代码块 {block.text} 的顺序是 {i + 1}")
                            self.dragging_block = None

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
            # 获取按键状态
            keys = pygame.key.get_pressed()
            # 根据按键状态更新人物位置
            if keys[pygame.K_UP]:
                self.char_rect.y -= 1
            if keys[pygame.K_DOWN]:
                self.char_rect.y += 1
            if keys[pygame.K_LEFT]:
                self.char_rect.x -= 1
            if keys[pygame.K_RIGHT]:
                self.char_rect.x += 1
            # 绘制背景和人物 bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb
            self.screen.blit(self.background, (0, 0))
            self.screen.blit(self.char, self.char_rect)
            # 更新被拖拽代码块的位置  dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd
            if self.dragging_block is not None:
                self.dragging_block.rect.x = event.pos[0] - offset_x
                self.dragging_block.rect.y = event.pos[1] - offset_y

            # 绘制界面
            for block in self.code_blocks:
                block.update()
                self.screen.blit(block.image, block.rect.topleft)
            pygame.display.flip()
            self.clock.tick(60)

if __name__ == "__main__":
    game = Game()
    game.run()
