from PIL import Image, ImageTk
import tkinter as tk


class Character:
    def __init__(self, root):
        self.root = root

        # 加载人物图片
        character_image = Image.open("character.jpg")
        self.character_photo = ImageTk.PhotoImage(character_image)

        # 创建一个Label，将人物图片设置为其内容
        self.character_label = tk.Label(self.root, image=self.character_photo)
        self.character_label.place(x=100, y=100)  # 设置人物初始位置

    def move_left(self):
        self.character_label.place(x=self.character_label.winfo_x() - 10, y=self.character_label.winfo_y())

    def move_right(self):
        self.character_label.place(x=self.character_label.winfo_x() + 10, y=self.character_label.winfo_y())

    def move_up(self):
        self.character_label.place(x=self.character_label.winfo_x(), y=self.character_label.winfo_y() - 10)

    def move_down(self):
        self.character_label.place(x=self.character_label.winfo_x(), y=self.character_label.winfo_y() + 10)


def background():
    # 创建主窗口
    root = tk.Tk()
    root.title("游戏")

    # 加载背景图片
    background_image = Image.open("background.jpg")
    background_photo = ImageTk.PhotoImage(background_image)

    # 创建一个Label，将背景图片设置为其内容
    background_label = tk.Label(root, image=background_photo)
    background_label.pack(fill=tk.BOTH, expand=True)

    # 创建人物对象
    character = Character(root)

    # 绑定键盘事件，控制人物移动
    root.bind("<Left>", lambda event: character.move_left())
    root.bind("<Right>", lambda event: character.move_right())
    root.bind("<Up>", lambda event: character.move_up())
    root.bind("<Down>", lambda event: character.move_down())

    # 运行主循环
    root.mainloop()
