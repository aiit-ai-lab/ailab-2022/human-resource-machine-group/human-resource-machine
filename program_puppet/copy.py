import pygame, sys, os
from pygame.locals import *
import threading

# 按钮类
class Button(object):
    # 构造函数
    def __init__(self, buttonUpImage, buttonDownImage, pos):
        # 按钮未按下的图片样式
        self.buttonUp = pygame.image.load(buttonUpImage).convert_alpha()
        # 按钮按下的图片样式
        self.buttonDown = pygame.image.load(buttonDownImage).convert_alpha()
        # 按钮在窗口中的位置
        self.pos = pos

    # 检查鼠标是否在按钮图片范围内
    def inButtonRange(self):
        # 获取鼠标的位置
        mouseX, mouseY = pygame.mouse.get_pos()
        x, y = self.pos
        w, h = self.buttonUp.get_size()
        inX = x - w / 2 < mouseX < x + w / 2
        inY = y - h / 2 < mouseY < y + h / 2
        return inX and inY

    # 在窗口中显示按钮
    def show(self, screen):
        w, h = self.buttonUp.get_size()
        x, y = self.pos
        # 根据鼠标位置变换样式
        if self.inButtonRange():
            screen.blit(self.buttonDown, (x - w / 2, y - h / 2))
        else:
            screen.blit(self.buttonUp, (x - w / 2, y - h / 2))


def block_interaction_thread(screen):
    block_size = 50
    block_x = 100
    block_y = 100
    dragging = False

    while True:
        for event in pygame.event.get():
            if event.type == MOUSEBUTTONDOWN:  # 当鼠标按下时
                mouse_x, mouse_y = event.pos
                if block_x < mouse_x < block_x + block_size and block_y < mouse_y < block_y + block_size:
                    dragging = True
            elif event.type == MOUSEBUTTONUP:  # 当鼠标松开时
                dragging = False
            elif event.type == MOUSEMOTION:  # 当鼠标移动时
                if dragging:
                    mouse_x, mouse_y = event.pos
                    block_x = mouse_x - block_size / 2
                    block_y = mouse_y - block_size / 2

        pygame.draw.rect(screen, (255, 0, 0), (block_x, block_y, block_size, block_size))  # 绘制方块
        pygame.display.update()  # 更新显示
class Character:
    def __init__(self, image_path, position):
        self.image = pygame.image.load(image_path).convert_alpha()
        self.position = position

    def draw(self, screen):
        screen.blit(self.image, self.position)

# 显示游戏主界面的函数
def showGameInterface(screen, interface, startGame, gameTips):
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == MOUSEBUTTONDOWN:
            # 如果点击的是“开始游戏”
            if startGame.inButtonRange():
                return 1
            # 如果点击的是“游戏说明”
            elif gameTips.inButtonRange():
                return 2
    # 绘制背景图片
    screen.blit(interface, (0, 0))
    # 显示“开始游戏”按钮
    startGame.show(screen)
    # 显示“游戏说明”按钮
    gameTips.show(screen)
    return 0

def showGame(screen,background):
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    # 绘制背景图片

    screen.blit(background, (0, 0))
    character = Character("char2.png", (300, 200))  # 创建角色对象在主线程中
    character.draw(screen)
    thread2 = threading.Thread(target=block_interaction_thread, args=(screen,))
    thread2.start()  # 启动另一个线程用于绘制方块和鼠标拖动
    #pygame.display.flip()  # 更新整个屏幕




random_array = []
def screen_drawing_thread(screen, background):
    global random_array

    screen.blit(background, (0, 0))
    pygame.display.flip()



def main():
    # 初始化pygame
    pygame.init()
    # 加载各项游戏资源（异常处理）
    try:
        # 设置游戏图标
        gameicon = pygame.image.load("BoxIcon.png")
        # # 展示游戏图标
        pygame.display.set_icon(gameicon)
        background = pygame.image.load("background.jpg")  # 背景
        screen = pygame.display.set_mode((1200, 675))
        button1 = Button('GameStartUp.png', 'GameStartDown.png', (200, 300))
        # “第一关”按钮
        buttonc1 = Button('Chapter1Up.png', 'Chapter1Down.png', (200, 175))
        # “第二关”按钮
        buttonc2 = Button('Chapter2Up.png', 'Chapter2Down.png', (200, 225))
        # “第三关”按钮
        buttonc3 = Button('Chapter3Up.png', 'Chapter3Down.png', (200, 275))
        # 主界面背景图片
        interface = pygame.image.load("begin.jpg")

        skinfilename = os.path.join('borgar.png')
        skin = pygame.image.load(skinfilename)
        skin = skin.convert()
    except pygame.error as msg:
        raise (SystemExit(msg))

    # 设置游戏界面标题
    pygame.display.set_caption('human resource machine~')
    flag = showGameInterface(screen, interface, buttonc1 ,button1)
    # 游戏主循环
    while True:
        if flag == 0:
            flag = showGameInterface(screen, interface, buttonc1,button1 )
        elif flag == 1:
            showGame(screen,background)
        elif flag == 2:
            screen.fill(skin.get_at((0, 0)))
        pygame.display.flip()





if __name__ == '__main__':
    main()