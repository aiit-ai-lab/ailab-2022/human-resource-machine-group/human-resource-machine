import pygame,sys
import cv2
import numpy as np


class Character:
    #角色类的定义
    def __init__(self, image_path, initial_position):
        #角色的属性
        # 加载角色图像
        char_image = pygame.image.load(image_path)
        char_width, char_height = char_image.get_size()
        self.char_images = [
            char_image.subsurface(j * (char_width // 3), i * ((char_height // 4)), char_width // 3, char_height // 4)
            for i in range(4) for j in range(3)]
        # 获取图像的矩形对象
        self.rect = self.char_images[0].get_rect()
        # 设置角色的初始位置
        self.rect.topleft = initial_position
        # 设置角色的移动速度
        self.speed = 4
        self.current_image = 0
        self.direction = None


    def move(self,output_image):
        #角色移动的方法
        global bg_x ,bg_y,buffer
        buffer=buffer+1
        if self.direction == 'left' and all(
                output_image[self.rect.y + y+14-bg_y, self.rect.x+14 - bg_x] == 0 for y in range(1,4)):
            if self.rect.x > 150 or (bg_x >= 0 and self.rect.x >= 0):
                self.rect.x -= self.speed
            elif bg_x <= -1:
                bg_x += self.speed
            if buffer%5 == 0:
                self.current_image = (self.current_image + 1) % 3 + 3  # 循环切换左走的动作
        elif self.direction == 'right' and all(
                output_image[self.rect.y + y+14-bg_y, self.rect.x + 18 - bg_x] == 0 for y in range(1,4)):
            if self.rect.x < 450 or (bg_x <= -400 and self.rect.x <= 568):
                self.rect.x += self.speed
            elif bg_x >= -399:
                bg_x -= self.speed
            if buffer%5 == 0:
                self.current_image = (self.current_image + 1) % 3 + 6  # 循环切换右走的动作
        elif self.direction == 'up' and all(
                output_image[self.rect.y+14 - bg_y, self.rect.x + x + 14 - bg_x] == 0 for x in range(1,4)):
            if self.rect.y >= 150 or (bg_y >= 0 and self.rect.y >= 0):
                self.rect.y -= self.speed
            elif bg_y <= -1:
                bg_y += self.speed
            if buffer % 5 == 0:
                self.current_image = (self.current_image + 1) % 3 + 9    # 循环切换上走的动作
        elif self.direction == 'down'and all(
                output_image[self.rect.y+18 - bg_y, self.rect.x + x + 14 - bg_x] == 0 for x in range(1,4)):
            if self.rect.y <= 450 or (bg_y <= -400 and self.rect.y <= 568):
                self.rect.y += self.speed
            elif bg_y >= -399:
                bg_y -= self.speed
            if buffer % 5 == 0:
                self.current_image = (self.current_image + 1) % 3   # 循环切换下走的动作
    def draw(self, screen):
        # 在屏幕上绘制角色
        screen.blit(self.char_images[self.current_image], self.rect)

class Poster:
    def __init__(self, image_path, position):
        self.image = pygame.image.load(image_path)
        self.rect = self.image.get_rect()
        self.rect.topleft = position

    def draw(self, screen):
        screen.blit(self.image, self.rect)


def read_dialog_from_file(filename):
    with open(filename, 'r', encoding='utf-8') as file:
        dialog_text = file.read()  # 读取文件中的所有内容
        dialogs = dialog_text.split("\n")  # 按&字符分割对话
        dialogs = [dialog.strip() for dialog in dialogs if dialog.strip()]  # 去掉每个对话的首尾空格，同时过滤掉空对话
    return dialogs

def create_dialog_surface(text, pos, frame_color, text_color):
    font = pygame.font.Font("talk.ttf", 20)  # 设置字体和大小
    dialog_text_lines = []
    temp_line = ''
    max_width = 300

    for char in text:
        temp_line += char
        if font.size(temp_line)[0] > max_width:
            dialog_text_lines.append(temp_line[:-1])
            temp_line = char
    dialog_text_lines.append(temp_line)

    text_height = font.get_linesize() * len(dialog_text_lines)

    dialog_width = 320
    dialog_height = text_height + 20

    dialog_surface = pygame.Surface((dialog_width, dialog_height), pygame.SRCALPHA)
    dialog_surface.set_alpha(200)
    pygame.draw.rect(dialog_surface, frame_color, (0, 0, dialog_width, dialog_height), border_radius=10)

    y = 10
    for line in dialog_text_lines:
        dialog_text = font.render(line, True, text_color)
        dialog_surface.blit(dialog_text, (10, y))
        y += font.get_linesize()

    return dialog_surface, (pos[0], pos[1] - dialog_height)
class Door:
    #传送门的属性
    def __init__(self, image_prefix, image_count, position):
        self.images = [pygame.image.load(f"rpg/{image_prefix}{i}.png") for i in range(0, image_count + 1)]
        self.rect = self.images[0].get_rect()
        self.rect.topleft = position
        self.current_image = 0

    def animate(self):
        #传送门的动态演示
        self.current_image = (self.current_image + 1) % len(self.images)  # 循环切换图像

    def reset(self, position):
        # 重置传送门位置
        self.rect.topleft = position

    def draw(self, screen):
        # 在屏幕上绘制传送门
        screen.blit(self.images[self.current_image], self.rect)

#按钮
class Button(object):
    # 构造函数
    def __init__(self, buttonUpImage, buttonDownImage, pos):
        # 按钮未按下的图片样式
        self.buttonUp = pygame.image.load(buttonUpImage)
        # 按钮按下的图片样式
        self.buttonDown = pygame.image.load(buttonDownImage)
        # 按钮在窗口中的位置
        self.pos = pos
    # 检查鼠标是否在按钮图片范围内
    def inButtonRange(self):
        # 获取鼠标的位置
        mouseX, mouseY = pygame.mouse.get_pos()
        x, y = self.pos
        w, h = self.buttonUp.get_size()
        inX = x - w / 2 < mouseX < x + w / 2
        inY = y - h / 2 < mouseY < y + h / 2
        return inX and inY
    # 在窗口中显示按钮
    def show(self, screen):
        w, h = self.buttonUp.get_size()
        x, y = self.pos
        # 根据鼠标位置变换样式
        if self.inButtonRange():
            screen.blit(self.buttonDown, (x - w / 2, y - h / 2))
        else:
            screen.blit(self.buttonUp, (x - w / 2, y - h / 2))


def showGameInterface(screen,interface,interface2,game_1,game_2,game_3):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if game_1.inButtonRange():
                return True
        screen.blit(interface, (0, 0))
        screen.blit(interface2, (20, 80))
        game_1.show(screen)
        game_2.show(screen)
        game_3.show(screen)
    pygame.display.flip()


def rpg_main():
    #程序入口开始执行
    #属性的素材的加载
    global bg_x,bg_y
    global buffer
    buffer=0
    flag=0
    pygame.init()
    screen_width = 600
    screen_height = 600


    #图片的加载
    clock = pygame.time.Clock()
    #开始游戏界面资源的加载
    button_1 = Button('start_down.png', 'start_up.png', (290, 210))
    button_2 = Button('content_down.png', 'content_up.png', (290, 300))
    button_3 = Button('out_down.png', 'out_up.png', (290, 390))
    interface = pygame.image.load("backhong.png")
    interface2 = pygame.image.load("juanzi1.png")


    #游戏开始后所需资源的加载
    background = pygame.image.load('openbig4.jpg')
    background1 = pygame.image.load('openbig5.jpg')
    background2 = pygame.image.load('openbig3.jpg')
    background3 = pygame.image.load('openbig1.jpg')
    background4 = pygame.image.load('openbig2.jpg')
    bg_width, bg_height = background.get_size()
    bg_x = (screen_width - bg_width) // 2
    bg_y = (screen_height - bg_height) // 2
    screen = pygame.display.set_mode((screen_width, screen_height))



    #数组的导入实现与图片对应
    try:
        output_image = np.load('edge_big4.npy')
        output_image1 = np.load('edge_big5.npy')
        output_image2 = np.load('edge_big3.npy')
        output_image3 = np.load('edge_big1.npy')
        output_image4 = np.load('edge_big2.npy')


        print("发现已保存的数组文件 'output_image.npy'，将其读入 output_image1 数组中。")
    except FileNotFoundError:
        image = cv2.imread('edge_big2.jpg', cv2.IMREAD_GRAYSCALE)
        edges = cv2.Canny(image, 100, 200)
        output_image = np.zeros_like(image)
        np.save('edge_big9.npy', output_image)
        print("未找到已保存的数组文件 'output_image.npy'，将创建新数组 output_image1。")
        for i in range(edges.shape[0]):
            for j in range(edges.shape[1]):
                if edges[i, j] > 0:
                    output_image[i, j] = 255
        cv2.imwrite('output_edge_points.jpg', output_image)
    character = Character("char1.png", (400, 300))



    #传送门实例化
    door_1 = Door("传送门特效_", 19, (200, 200))
    door_2 = Door("传送门特效_", 19, (400, 400))
    door_3 = Door("传送门特效_", 19, (400, 400))
    door_4 = Door("传送门特效_", 19, (400, 400))

    dialog_index = 0  # 用于按顺序显示对话的索引值
    dialogs = read_dialog_from_file('story/dialog4.txt')  # 将对话从文件中读取出来保存到dialogs中
    dialogsb = dialogs
    dialogs1 = read_dialog_from_file('story/dialog5.txt')
    dialogs2 = read_dialog_from_file('story/dialog3.txt')
    dialogs3 = read_dialog_from_file('story/dialog2.txt')
    dialogs4 = read_dialog_from_file('story/dialog1.txt')

    while True:
        running = showGameInterface(screen,interface,interface2,button_1,button_2,button_3)
        if running == 1:
            break

    #正式进入循环游戏正式开始

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    character.direction = 'left'
                elif event.key == pygame.K_RIGHT:
                    character.direction = 'right'
                elif event.key == pygame.K_UP:
                    character.direction = 'up'
                elif event.key == pygame.K_DOWN:
                    character.direction = 'down'
            if event.type == pygame.KEYUP:
                character.direction = None
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:  # 鼠标左键点击
                flag += 1
                if flag <= len(dialogs):  # 只要flag的值没有超过对话的数量
                    dialog_index = flag - 1  # 更新dialog_index将对话的值指向下一个
                print(flag)

        #传送门显示
        screen.blit(background, (bg_x, bg_y))

        if background == background1:
            posterp = (bg_x + 480, bg_y + 580)
            poster = Poster("person/char_5.png", posterp)
            poster.draw(screen)
            if character.rect.colliderect(door_1.rect):
                flag = 0
                background = backgroundb
                output_image = output_imageb
                bg_x = -400
                bg_y = 1
                dialogs = dialogsb
                character.rect.topleft = (500, 200)
            door_1.animate()
            door_1.reset((140 + bg_x, 140 + bg_y))
            door_1.draw(screen)


        elif background == background2:
            posterp = (bg_x + 480, bg_y + 580)
            poster = Poster("person/char_3.png", posterp)
            poster.draw(screen)
            if character.rect.colliderect(door_2.rect):
                background = backgroundb
                output_image = output_imageb
                dialogs = dialogsb
                character.rect.topleft = (270,110)
            door_2.animate()
            door_2.reset((340 + bg_x, 1 + bg_y))
            door_2.draw(screen)


        elif background == background3:
            posterp = (bg_x + 480, bg_y + 580)
            poster = Poster("person/char_1.png", posterp)
            poster.draw(screen)
            if character.rect.colliderect(door_3.rect):
                background = backgroundb
                output_image = output_imageb
                dialogs = dialogsb
                character.rect.topleft = (80, 180)
            door_3.animate()
            door_3.reset((2 + bg_x, 430 + bg_y))
            door_3.draw(screen)


        elif background == background4:
            posterp = (bg_x + 480, bg_y + 580)
            poster = Poster("person/char_2.png", posterp)
            poster.draw(screen)
            if character.rect.colliderect(door_4.rect):
                background = backgroundb
                output_image = output_imageb
                bg_x = -1
                bg_y = -250
                dialogs = dialogsb
                character.rect.topleft = (30,470)
            door_4.animate()
            door_4.reset((70 + bg_x, 2 + bg_y))
            door_4.draw(screen)


        else:
            posterp = (bg_x + 480, bg_y + 580)
            poster = Poster("person/char_4.png", posterp)
            poster.draw(screen)
            if character.rect.colliderect(door_1.rect):
                flag = 0
                backgroundb = background
                background = background1  # 背景切换到background1
                output_imageb = output_image
                output_image = output_image1
                dialogs = dialogs1
                bg_x=-1
                bg_y=-1
                character.rect.topleft = (240+bg_x,190+bg_y)
            door_1.animate()
            door_1.reset((920 + bg_x, 110 + bg_y))
            door_1.draw(screen)

            if character.rect.colliderect(door_2.rect):
                flag = 0
                backgroundb = background
                background = background2
                output_imageb = output_image
                output_image = output_image2
                dialogs = dialogs2
                character.rect.topleft = (360+bg_x, 80+bg_y)
            door_2.animate()
            door_2.reset((540 + bg_x, 2 + bg_y))
            door_2.draw(screen)

            if character.rect.colliderect(door_3.rect):
                flag = 0
                backgroundb = background
                background = background3
                output_imageb=output_image
                output_image = output_image3
                dialogs = dialogs3
                character.rect.topleft = (30, 420)
            door_3.animate()
            door_3.reset((3 + bg_x, 180 + bg_y))
            door_3.draw(screen)

            if character.rect.colliderect(door_4.rect):
                flag = 0
                backgroundb = background
                background = background4
                bg_x = -1
                bg_y = -1
                output_imageb=output_image
                output_image = output_image4
                dialogs = dialogs4
                character.rect.topleft = (90, 90)
            door_4.animate()
            door_4.reset((3 + bg_x, 780 + bg_y))
            door_4.draw(screen)

        if character.rect.colliderect(poster.rect):
            if 0 <= dialog_index < len(dialogs):  # 检查索引值是否合理
                if dialog_index % 2==0:
                    frame_color = (200, 181, 168)
                    text_color = (58, 53, 48)
                    dialog_text = dialogs[dialog_index]
                    dialog_surface, dialog_pos = create_dialog_surface(dialog_text, posterp, frame_color, text_color)
                    screen.blit(dialog_surface, dialog_pos)
                else:
                    frame_color = (180, 200, 200)
                    text_color = (78, 63, 38)
                    dialog_text = dialogs[dialog_index]
                    dialog_surface, dialog_pos = create_dialog_surface(dialog_text, character.rect, frame_color, text_color)
                    screen.blit(dialog_surface, dialog_pos)

        character.move(output_image)
        character.draw(screen)
        clock.tick(60)
        pygame.display.flip()











if __name__ == '__main__':
    rpg_main()