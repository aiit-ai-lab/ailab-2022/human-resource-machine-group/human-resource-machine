import pygame

# 定义代码块的类
class CodeBlock(pygame.sprite.Sprite):
    def __init__(self, text, pos):
        super().__init__()
        self.image = pygame.Surface((100, 35))
        if text == "copyfrom 2":
            self.image.fill((196, 107, 87))
        elif text == "jump 4":
            self.image.fill((142, 139, 184))
        else:
            self.image.fill((156, 187, 93))
        self.rect = self.image.get_rect()
        self.rect.topleft = pos
        self.text = text
        self.is_dragging = False
        self.has_jump = False

    def update(self):
        font = pygame.font.Font("char.ttf", 18)
        if self.text == "copyfrom 2":
            text_color = (101, 33, 17)
            block_color = (196, 107, 87)
        elif self.text == "jump 4":
            text_color = (50, 54, 92)
            block_color = (142, 139, 184)
        else:
            text_color = (64, 87, 9)
            block_color = (156, 187, 93)
        self.image.fill(block_color)

        # 如果是空白代码块，不渲染文字
        if self.text != " ":
            text = font.render(self.text, False, text_color)
            self.image.blit(text, (5, 4))

class ScrollBar(pygame.sprite.Sprite):
    def __init__(self, pos, width, height, color):
        super().__init__()
        self.image = pygame.Surface((width, height))
        self.image.fill(color)
        self.rect = self.image.get_rect()
        self.rect.topleft = pos


def create_dialog_surface(text, pos):
    font = pygame.font.Font("talk.ttf", 20)  # 设置字体和大小
    dialog_text_lines = []
    temp_line = ''
    max_width = 300

    for char in text:
        temp_line += char
        if font.size(temp_line)[0] > max_width:
            dialog_text_lines.append(temp_line[:-1])
            temp_line = char
    dialog_text_lines.append(temp_line)

    text_height = font.get_linesize() * len(dialog_text_lines)

    dialog_width = 320
    dialog_height = text_height + 20

    dialog_surface = pygame.Surface((dialog_width, dialog_height), pygame.SRCALPHA)
    pygame.draw.rect(dialog_surface, (220,75,48, 180), (0, 0, dialog_width, dialog_height), border_radius=10)

    y = 10
    for line in dialog_text_lines:
        dialog_text = font.render(line, True, (255, 255, 255))
        dialog_surface.blit(dialog_text, (10, y))
        y += font.get_linesize()

    return dialog_surface, (pos[0], pos[1] - dialog_height)

pygame.init()
screen = pygame.display.set_mode((1200, 675))
background = pygame.image.load("background.jpg")

ground = pygame.image.load("back2.jpg")
boss_image = pygame.image.load('boss.png')
boss_rect = boss_image.get_rect()
boss_rect.topleft = (554, 93)

clock = pygame.time.Clock()

# 创建多个代码块
code_blocks = []
block = CodeBlock("inbox 1", (30, 40 * 1))
code_blocks.append(block)
block = CodeBlock("copyfrom 2", (30, 40 * 2))
code_blocks.append(block)
block = CodeBlock("outbox 3", (30, 40 * 3))
code_blocks.append(block)
block = CodeBlock("jump 4", (30, 40 * 4))
code_blocks.append(block)


scroll_bar = ScrollBar((887, 119), 298, 400, (188, 160, 139))
dragging_block = None  # 当前被拖拽的代码块
gunblocks1 = []
gunblocks = []  # 存放拖拽到屏幕右侧的代码块

running = True
while running:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:  # 左键按下，尝试拖拽代码块
                for block in code_blocks:
                    if block.rect.collidepoint(event.pos):
                        dragging_block = block
                        offset_x = event.pos[0] - dragging_block.rect.x
                        offset_y = event.pos[1] - dragging_block.rect.y
                        break

            elif event.button == 4 and gunblocks:
                if gunblocks[-1].rect.top>70:  # 滚轮向上滚动
                    scroll_bar.rect.y = min(scroll_bar.rect.y + 10, 675)  # 调整滚动条位置
                    for block in gunblocks:
                        block.rect.y = max(block.rect.y - 10, 0)  # 相对移动gunblocks
            elif event.button == 5 and gunblocks:
                if gunblocks[0].rect.top<657:  # 滚轮向下滚动
                    scroll_bar.rect.y = max(scroll_bar.rect.y - 10, 0)  # 调整滚动条位置
                    for block in gunblocks:
                        block.rect.y = min(block.rect.y + 10, 675)  # 相对移动gunblocks

        elif event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1 and dragging_block is not None:  # 左键释放，停止拖拽
                dragging_block.rect.topleft=(887,60+len(gunblocks)*45)
                dragging_block.is_dragging = True
                if dragging_block.rect.right >= 900:  # 如果代码块拖到屏幕最右侧
                    gunblocks1.append(dragging_block)
                    print(f"代码块 {gunblocks1}")
                    sorted_blocks = sorted(gunblocks1, key=lambda block: block.rect.top)
                    gunblocks = sorted_blocks
                    for i, block in enumerate(sorted_blocks):
                        print(f"代码块 {block.text} 的顺序是 {i + 1}")
                dragging_block = None

    mouse_x, mouse_y = pygame.mouse.get_pos()

    if dragging_block is not None:  # 更新被拖拽代码块的位置
        dragging_block.rect.x = event.pos[0] - offset_x
        dragging_block.rect.y = event.pos[1] - offset_y


    # 绘制界面
    screen.blit(background, (0, 0))
    screen.blit(scroll_bar.image, scroll_bar.rect.topleft)
    screen.blit(boss_image, boss_rect)

    black = (0, 0, 0)
    white = (255, 255, 255)

    font = pygame.font.Font("title1.ttf", 36)
    text_render = font.render("欢迎进入游戏", True, white)
    screen.blit(text_render, (300, 300))

    if boss_rect.collidepoint(pygame.mouse.get_pos()):
        # 显示绿色对话框
        # 可以通过绘制矩形来实现，如：
        dialog_text = "Hello,world!中国在这部分代码中 ，我们通过分行后的文本行数计算了整个对话框应有的高度 ，并为对话框设置了固定的宽 度和根据文本高度计算的高度"
        dialog_surface, dialog_pos = create_dialog_surface(dialog_text, (250, 200))
        screen.blit(dialog_surface, dialog_pos)



    if gunblocks:
        for i, block in enumerate(gunblocks):
            font = pygame.font.Font("char.ttf", 18)
            text = font.render(str(i+1), True, (255, 255, 255))
            pygame.draw.rect(screen, (0, 0, 0), (block.rect.x - 30, block.rect.y - 25, 30, 30))
            screen.blit(text, (block.rect.x - 20, block.rect.y - 25))

    for block in code_blocks:
        block.update()
        screen.blit(block.image, block.rect.topleft)

    screen.blit(ground, (887, 0))

    pygame.display.flip()
    clock.tick(60)
