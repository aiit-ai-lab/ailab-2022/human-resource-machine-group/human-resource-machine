import pygame
import numpy as np
import time

step = 0
flag = False

class FixRect(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface([35, 35])
        self.image.fill((196, 107, 87))  # 红色填充
        self.number = 0
        self.rect = self.image.get_rect()

    def draw(self, screen):
        font = pygame.font.Font(None, 27)
        text_surface = font.render(str(number), True, (0, 0, 0))  # 数字颜色为深色
        text_rect = text_surface.get_rect(center=(self.image.get_width() / 2, self.image.get_height() / 2))
        self.image.blit(text_surface, text_rect.topleft)

        # # 如果有数字的话
            # font = pygame.font.Font(None, 27)
            # text = font.render(str(self.number), True, (0, 0, 0))  # 闭hashCode
            # screen.blit(text, (self.rect.x + self.rect.width / 2, self.rect.y + self.rect.height / 2))



    def move(self, dx, dy):
        self.rect.x = dx
        self.rect.y = dy

    def set_number(self, number):
        self.number = number

class CodeBlock(pygame.sprite.Sprite):
    def __init__(self, text, pos):
        super().__init__()
        self.image = pygame.Surface((100, 35))
        if text == "copyfrom 2":
            self.image.fill((196, 107, 87))
        elif text == "jump 4":
            self.image.fill((142, 139, 184))
        elif text == "      ":
            self.image.fill((142, 139, 184))
        elif text == "copyto 5":
            self.image.fill((196, 107, 87))
        elif text == "add 6":
            self.image.fill((188, 138, 105))
        else:
            self.image.fill((156, 187, 93))
        self.rect = self.image.get_rect()
        self.rect.topleft = pos
        self.original_position = pos
        self.text = text
        self.has_sub_block = False
        self.linked_block = None
        self.linked_fix_rect = None
        self.jump_sum = None

    def upui(self):
        font = pygame.font.Font("char.ttf", 18)
        if self.text == "copyfrom 2":
            text = font.render(self.text, False, (101, 33, 17))
        elif self.text == "jump 4":
            text = font.render(self.text, False, (50, 54, 92))
        elif self.text == "copyto 5":
            text = font.render(self.text, False, (101, 33, 17))
        elif self.text == "add 6":
            text = font.render(self.text, False, (81, 53, 23))
        else:
            text = font.render(self.text, False, (64, 87, 9))
        self.image.blit(text, (5, 4))

    def updata(self):
        if self.text == "inbox 1":
            code_y = 1
        elif self.text == "copyfrom 2":
            code_y = 3
        elif self.text == "outbox 3":
            code_y = 2
        elif self.text == "jump 4":
            code_y = 5
        elif self.text == "copyto 5":
            code_y = 4
        elif self.text == "add 6":
            code_y = 6
        elif self.text == "      ":
            code_y = -4
        if dragging_block.rect.right >= goal_p :
            if dragging_block.text=="jump 4" and self.has_sub_block == False:
                x,y=dragging_block.rect.topleft
                new_sub_block = CodeBlock("      ", (x,y+40))  # 位于下一个位置
                code_blocks.append(new_sub_block)
                dragging_block.linked_block = new_sub_block
                self.has_sub_block = True


            if self.text == "copyto 5" and self.has_sub_block == False:
                fix_rect = FixRect()
                x, y = dragging_block.rect.topleft
                fix_rect.rect.topleft = (x + 110, y)
                fix_rect_group.add(fix_rect)
                self.linked_fix_rect = fix_rect
                self.has_sub_block = True
            elif self.linked_fix_rect is not None:
                x, y = self.rect.topleft
                self.linked_fix_rect.move(x + 110, y)


            new_block = CodeBlock(self.text, (30, 40 * code_y))

            code_blocks.append(new_block)


        else:
            self.rect.topleft = self.original_position
        # if self.text== "copyto 5" :
        #     print('kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk')
        #     x, y = dragging_block.rect.topleft
        #     fix_rect.move(x + 110, y)

class ScrollBar(pygame.sprite.Sprite):
    def __init__(self, pos, width, height, color):
        super().__init__()
        self.image = pygame.Surface((width, height))
        self.image.fill(color)
        self.rect = self.image.get_rect()
        self.rect.topleft = pos

box=0
needle = 0
drawer1 = None
class Character(pygame.sprite.Sprite):
    def __init__(self, image, pos):
        super().__init__()
        self.image = pygame.image.load(image)
        self.rect = self.image.get_rect()
        self.rect.topleft = pos
        self.speed = 2  # 每次游戏循环移动的像素数
        self.holding_block = None  # 新增属性，初始时没有持有任何方块
        self.target_pos = None

    def move_to(self, pos):
        self.target_pos = pos

    def update(self):
        global box,drawer1
        if self.target_pos:
            dx = self.target_pos[0] - self.rect.x
            dy = self.target_pos[1] - self.rect.y

            if abs(dx) > self.speed or abs(dy) > self.speed:
                self.rect.x += self.speed if dx > 0 else - self.speed if dx < 0 else 0
                self.rect.y += self.speed if dy > 0 else - self.speed if dy < 0 else 0
            else:
                self.rect.x = self.target_pos[0]
                self.rect.y = self.target_pos[1]
                self.target_pos = None

        if self.rect.topleft == (560, 300) and self.holding_block:#入库
            self.drop_block(640, 350+(box-1)*40)  # 放下方块
            self.holding_block = None

        if pygame.sprite.collide_rect(self, blocks.sprites()[box]) and self.holding_block ==None:#取箱
            print(f'ssssssssss{blocks.sprites()[box]}')
            self.pick_up_block(blocks.sprites()[box])
            if box<len(blocks)-1:
                box +=1
            if self.holding_block is not None:
                print(f'hhhhhhhh{self.holding_block.number}')



        if drawer1 is not None:
            if pygame.sprite.collide_rect(self,drawer1) and self.holding_block == None:  # 取箱
                self.pick_up_block(drawer1)
            # print(f'ssssssssss{blocks.sprites()[box]}')
            # self.pick_up_block(blocks.sprites()[box])
            # if pygame.sprite.collide_rect(self, drawer1) :#取箱
            #     print(f'ddddddddd{drawer1.number}')
            #     drawer1.number=10
            #     self.pick_up_block(drawer1)


        if self.holding_block:
            # 如果持有方块，更新方块位置
            self.holding_block.rect.center = self.rect.center

        if self.rect.topleft == (310, 250) and self.holding_block:#复制

            self.drop_block(310, 350)
            time.sleep(1)
            new_block = self.holding_block.clone()
            drawer1 = new_block
            #new_block.rect.center= (310, 250)  # 设置新箱子的位置
            blocks.add(new_block)


    def control(self):
        global step,flag,box,needle
        if self.target_pos == None and flag == True:
            current_block = sorted_blocks
            print(f'步伐{step}')
            print(current_block)

            if current_block[step].text == "inbox 1":
                self.move_to(blocks.sprites()[box].rect.topleft)  # 移动到第一个方块的位置




            elif current_block[step].text == "outbox 3":
                drop_position = (560, 300)  # 这里需要根据你的游戏逻辑来设置
                self.move_to(drop_position)


            elif current_block[step].text == "copyto 5":
                self.move_to((310, 250))


            elif current_block[step].text == "copyfrom 2":
                drop_position = (310, 350)  # 这里需要根据你的游戏逻辑来设置
                self.move_to(drop_position)



            elif current_block[step].text == "jump 4":
                jump_index = current_block[step].jump
                print(f'来到了在这里{jump_index}')
                if jump_index is not None:
                    step = jump_index

            step += 1

            if len(sorted_blocks) <= step:
                step = 0
                flag = False


    def pick_up_block(self, block):
        """ 拿起一个方块 """
        self.holding_block = block

    def drop_block(self,x,y):
        """ 放下方块 """
        self.holding_block.rect.topleft = (x,y)



    def draw(self, screen):
        pygame.draw.rect(screen, (169, 169, 169), self.rect)  # 绘制滚动列表背景
        # 绘制滚动列表内的内容（代码块）
        for block in code_blocks:
            if self.rect.collidepoint(block.rect.centerx, block.rect.centery):
                screen.blit(block.image, block.rect.topleft)

class NumberBlock(pygame.sprite.Sprite):
    def __init__(self, number, pos):
        super().__init__()
        self.image = pygame.Surface([30, 30])
        self.image.fill((173, 208, 124))  # 绿色填充
        self.rect = self.image.get_rect()
        self.rect.topleft = pos
        self.number = number


        # 在方块中心显示数字
        font = pygame.font.Font(None, 27)
        text = font.render(str(number), 1, (79, 97 , 38))  # 数字颜色为深色
        textpos = text.get_rect(centerx=self.image.get_width()/2, centery=self.image.get_height()/2)
        self.image.blit(text, textpos)

    def clone(self):
        new_block = NumberBlock(self.number, self.rect.topleft)  # 创建一个新的NumberBlock实例
        return new_block


def paint():
    for block in code_blocks:
        block.upui()
        screen.blit(block.image, block.rect.topleft)
        if block.linked_block:  # 如果有连接的块
            module1 = block.rect.topleft
            module2 = block.linked_block.rect.topleft

            # 增加起始点和结束点的 x 坐标
            start_x = module1[0] + block.rect.width
            end_x = module2[0] + block.linked_block.rect.width

            # 控制点可以位于两个模块的中点，并且y轴上稍微偏离
            control_point1 = (start_x + int(abs(module1[1] - module2[1])/5+10),
                              (module1[1] + module2[1] + module_size) // 2)

            t = np.linspace(0, 1, 50000)
            x_curve = (1 - t) ** 2 * start_x + 2 * (1 - t) * t * control_point1[0] + t ** 2 * end_x
            y_curve = (1 - t) ** 2 * (module1[1] + module_size // 2) + 2 * (1 - t) * t * control_point1[1] + t ** 2 * (module2[1] + module_size // 2)

            curve_points = list(zip(x_curve, y_curve))
            pygame.draw.lines(screen, (142, 139, 184), False, curve_points, 3)



pygame.init()
screen = pygame.display.set_mode((1200, 675))#大小
scroll_bar = ScrollBar((887, -500), 298, 5000, (188, 160, 139))
background = pygame.image.load("background.jpg")#背景
ground = pygame.image.load("ground.jpg")#背景
character = Character("char.png", (300,200))#人物
hand_image = pygame.image.load('hand.png')
##执行按钮
play_button_image = pygame.image.load("play_button.jpg")
play_button_rect = play_button_image.get_rect()
play_button_rect.topleft = (330, 600)
##
clock = pygame.time.Clock()
goal_p = 900

# 创建多个代码块
code_blocks = []
block = CodeBlock(f"inbox 1", (30, 40 * 1))
code_blocks.append(block)
block = CodeBlock(f"outbox 3", (30, 40 * 2))
code_blocks.append(block)
block = CodeBlock(f"copyfrom 2", (30, 40 * 3))
code_blocks.append(block)
block = CodeBlock(f"copyto 5", (30, 40 * 4))
code_blocks.append(block)
block = CodeBlock(f"add 6", (30, 40 * 6))
code_blocks.append(block)
block = CodeBlock(f"jump 4", (30, 40 * 5))

code_blocks.append(block)

# 目标代码块
goal_blocks = []
sorted_blocks =[]

# 创建一个存放所有方块的组
blocks = pygame.sprite.Group()
fix_rect_group = pygame.sprite.Group()
# 在屏幕上创建一些数字方块
numbers = [1, 2, 3, 4, 5]  # 要显示的数字
start_pos = (123, 320)  # 第一个方块的位置
spacing = 40  # 方块之间的垂直间距

for i, number in enumerate(numbers):
    block1 = NumberBlock(number, (start_pos[0], start_pos[1] + i * spacing))
    blocks.add(block1)

dragging_block = None  # 当前被拖拽的代码块
gunblocks = []


# 单独的函数处理事件
def handle_events():
    global running, gamerun, dragging_block, offset_x, offset_y,flag,step,sorted_blocks,gunblocks
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:  # 左键按下，尝试拖拽代码块
                for block in code_blocks:
                    if block.rect.collidepoint(event.pos):
                        dragging_block = block
                        offset_x = event.pos[0] - dragging_block.rect.x
                        offset_y = event.pos[1] - dragging_block.rect.y
                        goal_blocks.clear()
                        break
                if play_button_rect.collidepoint(event.pos):
                    gamerun = False
                    # 按钮被点击，执行操作
                    flag = True
                    # if block.rect.right >= screen.get_width() * 2 // 3:
                    # print(f"代码块 {block.text} 的顺序是 {i + 1}")
                print('************************************************')
            elif event.button == 4 and gunblocks:
                if gunblocks[-1].rect.top > 70:
                    for block in gunblocks:
                        block.rect.y = block.rect.y - 10  # 相对移动gunblocks
                        print(f"gunblock {block.text} 的位置是 {block.rect.y}")
                    print('-----------------------------------------------')
            elif event.button == 5 and gunblocks:  # 滚轮向下滚动
                if gunblocks[0].rect.top < 657:
                    for block in gunblocks:
                        block.rect.y = block.rect.y + 10 # 相对移动gunblocks
                        print(f"gunblock {block.text} 的位置是 {block.rect.y}")
                    print('-----------------------------------------------')

        elif event.type == pygame.MOUSEBUTTONUP:
            if dragging_block is not None:  # 左键释放，停止拖拽

                flag = False  # 停停停停停停停停
                step = 0
                dragging_block.updata()
                if dragging_block.rect.right >= goal_p:  # 如果代码块拖到屏幕最右侧
                    for block in code_blocks:
                        if block.rect.right >= goal_p:
                            goal_blocks.append(block)
                            gunblocks = goal_blocks
                            #print(f"代码块111 {gunblocks}")
                    sorted_blocks = sorted(goal_blocks, key=lambda block: block.rect.top)
                    for i, block1 in enumerate(sorted_blocks):
                        # i for i, block in enumerate(sorted_blocks) if current_block[step].linked_block == block
                        block1.rect.topleft = (900, 60 + i * 45)
                        print(f"代码块 {block1.text} 的顺序是 {i + 1}")


                    for i, blockx in enumerate(sorted_blocks):
                        if blockx.linked_block is not None:
                            for j, blocko in enumerate(sorted_blocks):
                                if blocko == blockx.linked_block:
                                    blockx.jump = j

                    print('------------------------------------------------')
                dragging_block = None
        elif event.type == pygame.MOUSEMOTION:  # 鼠标移动事件
            if dragging_block is not None:  # 如果有代码块正在被拖拽
                dragging_block.rect.x = event.pos[0] - offset_x
                dragging_block.rect.y = event.pos[1] - offset_y



running = True
gamerun = True

offset_x = 0
offset_y = 0

while running:
    handle_events()



    # 绘制界面 c
    character.update()
    character.control()

    #pygame.draw.rect(screen, (188, 160, 139), rect)请在其基础上实现一个从y=900，到屏幕最右侧滚动列表，可将可拖动代码块放入其中，代码块足够多及代码块超过10个时产生，可上下翻动的列表条

    screen.blit(background, (0, 0))

    screen.blit(play_button_image, play_button_rect)
    screen.blit(character.image, character.rect)
    character.update()
    pygame.draw.line(screen, (0, 0, 0), (goal_p, 0),
                     (goal_p, screen.get_height()), 2)
    module_size=50
    pygame.draw.rect(screen, (255, 0, 0), (300, 340,60, 60), 2)
    pygame.draw.rect(screen, (0, 255, 0), (365, 340, 60, 60), 2)
    pygame.draw.rect(screen, (0, 0, 255), (430, 340, 60, 60), 2)

    screen.blit(scroll_bar.image, scroll_bar.rect.topleft)
    paint()

    screen.blit(ground, (887, 0))
    if len(sorted_blocks)>0:
        if step==0 and step==True:
            screen.blit(hand_image, (sorted_blocks[step].rect.x - 80, sorted_blocks[step].rect.y - 30))
        elif step==len(sorted_blocks):
            screen.blit(hand_image, (sorted_blocks[len(sorted_blocks)-1].rect.x - 80, sorted_blocks[len(sorted_blocks)-1].rect.y - 30))
        else:
            screen.blit(hand_image, (sorted_blocks[step-1].rect.x-80 , sorted_blocks[step-1].rect.y - 30))
    blocks.draw(screen)

    pygame.display.flip()
    clock.tick(600)

pygame.quit()#我希望实现滚动列表去容纳CodeBlock代码块，当代码块个数大于10个的时候，就会产生滚动列表的拖动条，使得容纳更多的代码块
