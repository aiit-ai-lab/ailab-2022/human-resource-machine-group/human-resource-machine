
import pygame, sys, os
from game_begins import *
import threading
from code_block import *

from pygame.locals import *
import numpy as np
import time
import random

directionf = 0
story = 0
to_do = 0
box=0
rk =1
needle = 0
storage_area=[None,None,None]
outbox = []
flag = False
step = 0
# 创建时钟对象
clock = pygame.time.Clock()
fps = 30  # 希望达到的帧率
class Character:
    def __init__(self, image_path, initial_position):
        # 加载角色图像
        char_image = pygame.image.load(image_path)
        char_width, char_height = char_image.get_size()
        self.char_images = [
            char_image.subsurface(j * (char_width // 3), i * ((char_height // 4)), char_width // 3, char_height // 4)
            for i in range(4) for j in range(3)]
        # 获取图像的矩形对象
        self.rect = self.char_images[0].get_rect()
        # 设置角色的初始位置
        self.rect.topleft = initial_position
        # 设置角色的移动速度
        self.speed = 15
        self.holding_block = None
        # 当前角色图像索引
        self.current_image = 0
        self.target_pos = None

    def move_to(self, pos):
        self.target_pos = pos
    def move(self, direction):
        global directionf,outbox
        directionf = direction
        if flag == True:
            clock.tick(fps)

        # 根据按键调整角色的位置
        if direction == 3:
            self.rect.x -= self.speed
            # if df % 2 == 0:
            self.current_image = (self.current_image + 1) % 3 + 3  # 循环切换左走的动作
        elif direction == 1:
            self.rect.x += self.speed
            # if df % 2 == 0:
            self.current_image = (self.current_image + 1) % 3 + 6  # 循环切换右走的动作
        elif direction == 4:
            self.rect.y -= self.speed
            # if df % 2 == 0:
            self.current_image = (self.current_image + 1) % 3 + 9    # 循环切换上走的动作
        elif direction == 2:
            self.rect.y += self.speed
            # if df % 2 == 0:
            self.current_image = (self.current_image + 1) % 3   # 循环切换下走的动作


    def update(self):
        global box,rk,to_do,step,direction,storage_area,flag                                         #8888888888888888888888888

        if self.target_pos:
            dx = self.target_pos[0] - self.rect.x
            dy = self.target_pos[1] - self.rect.y

            if abs(dx) > self.speed :
                self.move(1) if dx > 0 else self.move(3) if dx < 0 else 0
            elif abs(dy) > self.speed:
                self.move(2) if dy > 0 else self.move(4) if dy < 0 else 0
            else:
                self.rect.x = self.target_pos[0]
                self.rect.y = self.target_pos[1]
                self.target_pos = None

    #  ----------------     inbox     --------------
        if to_do == 1 and pygame.sprite.collide_rect(self, blocks.sprites()[box]):          # and self.holding_block ==None:#取箱
            if flag:
                print(f'-------------------------------------$$$$$${box}')
                self.pick_up_block(blocks.sprites()[box])
                if box < len(blocks)-1:
                    box += 1
                to_do = 0
                step +=1
            else:
                drop_position = ((300, 400))  # 这里需要根据你的游戏逻辑来设置
                self.move_to(drop_position)

    #  ----------------     outbox     --------------
        if to_do == 2 and self.rect.topleft == (560, 300) and self.holding_block:#入库
            self.drop_block(640, 350+(rk-1)*40)  # 放下方块
            rk = rk+1
            outbox.append(self.holding_block.number)     #kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk
            self.holding_block = None
            # if box >= len(blocks):#=================
            #     flag = False
            to_do = 0
            step += 1

    # --------------      copy from      ------------------
        if to_do == 4.1 and storage_area[0] is not None :
            if pygame.sprite.collide_rect(self, storage_area[0]) :  # 取箱特定位置1 2 3
                new_box = storage_area[0].clone()
                blocks.add(new_box)
                self.pick_up_block(new_box)

            to_do = 0
            if pygame.sprite.collide_rect(self, storage_area[0]):
                step += 1
        elif to_do == 4.2 and storage_area[1] is not None :
            if pygame.sprite.collide_rect(self, storage_area[1]) :
                new_box = storage_area[1].clone()
                blocks.add(new_box)
                self.pick_up_block(new_box)

            to_do = 0
            if pygame.sprite.collide_rect(self, storage_area[1]):
                step += 1
        elif to_do == 4.3 and storage_area[2] is not None :
            if pygame.sprite.collide_rect(self, storage_area[2]) :
                new_box = storage_area[2].clone()
                blocks.add(new_box)
                self.pick_up_block(new_box)
            to_do = 0
            if pygame.sprite.collide_rect(self, storage_area[2]):
                step += 1

    # ----------------       copy to      ------------------

        if to_do == 3 and self.holding_block :
            if self.rect.topleft == (310, 250) :#复制
                self.drop_block(310, 350)
                time.sleep(0.5)
                new_block = self.holding_block.clone()
                storage_area[0] = new_block                                                          #888888888888888888888888
                #new_block.rect.center= (310, 250)  # 设置新箱子的位置
                blocks.add(new_block)
                to_do = 0
                step +=1
            elif self.rect.topleft == (350, 250) :#复制
                self.drop_block(370, 350)
                time.sleep(0.5)
                new_block = self.holding_block.clone()
                storage_area[1] = new_block
                #new_block.rect.center= (310, 250)  # 设置新箱子的位置
                blocks.add(new_block)
                to_do = 0
                step +=1
            elif self.rect.topleft == (410, 250) :#复制
                self.drop_block(450, 350)
                time.sleep(0.5)
                new_block = self.holding_block.clone()
                storage_area[2] = new_block
                #new_block.rect.center= (310, 250)  # 设置新箱子的位置
                blocks.add(new_block)
                to_do = 0
                step +=1

    #  ---------------       add       -----------------
        if to_do == 5.1 and self.rect.topleft == (310,250):
            time.sleep(0.5)
            self.holding_block.add_number(storage_area[0])
            to_do = 0
            step += 1
        elif to_do == 5.2 and self.rect.topleft == (350,250):
            time.sleep(0.5)
            self.holding_block.add_number(storage_area[1])
            to_do = 0
            step += 1
        elif to_do == 5.3 and self.rect.topleft == (390,250):
            time.sleep(0.5)
            self.holding_block.add_number(storage_area[2])
            to_do = 0
            step += 1


        #   ---------------       mul       -----------------
        if to_do == 6.1 and self.rect.topleft == (310, 250):
            time.sleep(0.5)
            self.holding_block.mul_number(storage_area[0])
            to_do = 0
            step += 1
        elif to_do == 6.2 and self.rect.topleft == (350, 250):
            time.sleep(0.5)
            self.holding_block.mul_number(storage_area[1])
            to_do = 0
            step += 1
        elif to_do == 6.3 and self.rect.topleft == (390, 250):
            time.sleep(0.5)
            self.holding_block.mul_number(storage_area[2])
            to_do = 0
            step += 1

        #  ---------------       div       -----------------
        if to_do == 7.1 and self.rect.topleft == (310, 250):
            time.sleep(0.5)
            self.holding_block.div_number(storage_area[0])
            to_do = 0
            step += 1
        elif to_do == 7.2 and self.rect.topleft == (350, 250):
            time.sleep(0.5)
            self.holding_block.div_number(storage_area[1])
            to_do = 0
            step += 1
        elif to_do == 7.3 and self.rect.topleft == (390, 250):
            time.sleep(0.5)
            self.holding_block.div_number(storage_area[2])
            to_do = 0
            step += 1

        #  ---------------       sub       -----------------
        if to_do == 8.1 and self.rect.topleft == (310, 250):
            time.sleep(0.5)
            self.holding_block.sub_number(storage_area[0])
            to_do = 0
            step += 1
        elif to_do == 8.2 and self.rect.topleft == (350, 250):
            time.sleep(0.5)
            self.holding_block.sub_number(storage_area[1])
            to_do = 0
            step += 1
        elif to_do == 8.3 and self.rect.topleft == (390, 250):
            time.sleep(0.5)
            self.holding_block.sub_number(storage_area[2])
            to_do = 0
            step += 1

        #  ---------------       squ       -----------------
        if to_do == 9 and self.rect.topleft == (350, 250):
            time.sleep(0.5)
            self.holding_block.squ_number()
            to_do = 0
            step += 1







        if self.holding_block:     # 如果持有方块，更新方块位置
            x,y=self.rect.center
            if directionf == 1:
                self.holding_block.rect.center = (x+45, y+5)
            elif directionf == 3:
                self.holding_block.rect.center = (x-45, y+5)
            elif directionf == 2:
                self.holding_block.rect.center = (x, y-20)
            elif directionf == 4:
                self.holding_block.rect.center = (x, y-60)
            else:
                self.holding_block.rect.center = (x, y)




    def control(self):
        global step,flag,box,needle,sorted_blocks,to_do,hold
        sorted_blocks = commands()
        if self.target_pos == None and flag == True:
            current_block = commands()

            if len(current_block) <= step:
                flag = False
                step = 0

            print(f'步伐{step}')

            if flag == False and step == 0:
                drop_position = (500, 500)  # 这里需要根据你的游戏逻辑来设置
                self.move_to(drop_position)

            if flag == True:
                if current_block[step].text == " 取 箱":
                    to_do = 1
                    self.move_to(blocks.sprites()[box].rect.topleft)  # 移动到第一个方块的位置

                elif current_block[step].text == " 置 案":
                    to_do = 2
                    drop_position = (560, 300)  # 这里需要根据你的游戏逻辑来设置
                    self.move_to(drop_position)


                elif current_block[step].text == " 誉 至":
                    to_do = 3
                    if current_block[step].small_square.value == 1:
                        self.move_to((310, 250))
                    if current_block[step].small_square.value == 2:
                        self.move_to((350, 250))
                    if current_block[step].small_square.value == 3:
                        self.move_to((410, 250))




                elif current_block[step].text == " 抄 誉":
                    if current_block[step].small_square.value == 1:
                        to_do = 4.1
                        drop_position = (310, 350)  # 这里需要根据你的游戏逻辑来设置
                        hold = True
                        self.move_to(drop_position)
                    if current_block[step].small_square.value == 2:
                        to_do = 4.2
                        drop_position = (350, 350)  # 这里需要根据你的游戏逻辑来设置
                        hold = True
                        self.move_to(drop_position)
                    if current_block[step].small_square.value == 3:
                        to_do = 4.3
                        drop_position = (390, 350)  # 这里需要根据你的游戏逻辑来设置
                        hold = True
                        self.move_to(drop_position)

                elif current_block[step].text == " 相 和":
                    if current_block[step].small_square.value == 1:
                        to_do = 5.1
                        self.move_to((310, 250))
                    if current_block[step].small_square.value == 2:
                        to_do = 5.2
                        self.move_to((350, 250))
                    if current_block[step].small_square.value == 3:
                        to_do = 5.3
                        self.move_to((390, 250))

                elif current_block[step].text == " 乘 以":
                    if current_block[step].small_square.value == 1:
                        to_do = 6.1
                        self.move_to((310, 250))
                    if current_block[step].small_square.value == 2:
                        to_do = 6.2
                        self.move_to((350, 250))
                    if current_block[step].small_square.value == 3:
                        to_do = 6.3
                        self.move_to((390, 250))

                elif current_block[step].text == " 除 以":
                    if current_block[step].small_square.value == 1:
                        to_do = 7.1
                        self.move_to((310, 250))
                    if current_block[step].small_square.value == 2:
                        to_do = 7.2
                        self.move_to((350, 250))
                    if current_block[step].small_square.value == 3:
                        to_do = 7.3
                        self.move_to((390, 250))

                elif current_block[step].text == " 相 减":
                    if current_block[step].small_square.value == 1:
                        to_do = 8.1
                        self.move_to((310, 250))
                    if current_block[step].small_square.value == 2:
                        to_do = 8.2
                        self.move_to((350, 250))
                    if current_block[step].small_square.value == 3:
                        to_do = 8.3
                        self.move_to((390, 250))

                elif current_block[step].text == " 平 方":
                    if self.holding_block is not None:
                        to_do = 9
                        self.move_to((350, 250))

                elif current_block[step].text == " 复 始":
                    jump_index = current_block[step].jump
                    if jump_index is not None:
                        step = jump_index

                elif current_block[step].text == " 疑 零":
                    if self.holding_block.number == 0:
                        jump_index = current_block[step].jump
                        if jump_index is not None:
                            step = jump_index
                    else:
                        step +=1

                elif current_block[step].text == "      ":
                    step +=1
                    if len(current_block) == step:
                        step = 0
                        flag = False

        # step += 1

            # if len(current_block) <= step:
            #     step = 0
            #     flag = False


    def pick_up_block(self, block):
        """ 拿起一个方块 """
        if self.holding_block:  # 如果手上有箱子
            self.throw_away_block()
        self.holding_block = block

    def drop_block(self,x,y):
        """ 放下方块 """
        self.holding_block.rect.topleft = (x,y)

    def throw_away_block(self):
        """ 抛出方块 """
        original_x, original_y = self.holding_block.rect.topleft
        new_x = original_x - 50  # 向后移动50个像素
        while self.holding_block.rect.x > new_x:
            self.holding_block.rect.x -= 1  # 向后移动一个像素
        print(f'({self.holding_block.rect.x}, {self.holding_block.rect.y}) and {self.holding_block.number}')
        self.holding_block.rect = (-50, -50)
        self.holding_block = None

    def draw(self, screen):
        # 在屏幕上绘制角色
        screen.blit(self.char_images[self.current_image], self.rect)


def create_dialog_surface(text, pos,frame_color,text_color):
    font = pygame.font.Font("talk.ttf", 20)  # 设置字体和大小
    dialog_text_lines = []
    temp_line = ''
    max_width = 300

    for char in text:
        temp_line += char
        if font.size(temp_line)[0] > max_width:
            dialog_text_lines.append(temp_line[:-1])
            temp_line = char
    dialog_text_lines.append(temp_line)

    text_height = font.get_linesize() * len(dialog_text_lines)

    dialog_width = 320
    dialog_height = text_height + 20

    dialog_surface = pygame.Surface((dialog_width, dialog_height), pygame.SRCALPHA)
    dialog_surface.set_alpha(200)
    pygame.draw.rect(dialog_surface, frame_color, (0, 0, dialog_width, dialog_height), border_radius=10)

    y = 10
    for line in dialog_text_lines:
        dialog_text = font.render(line, True, text_color)
        dialog_surface.blit(dialog_text, (10, y))
        y += font.get_linesize()

    return dialog_surface, (pos[0], pos[1] - dialog_height)





class NumberBlock(pygame.sprite.Sprite):
    def __init__(self, number, pos):
        super().__init__()
        self.image = pygame.Surface([30, 30])
        self.image.fill((173, 208, 124))  # 绿色填充
        self.rect = self.image.get_rect()
        self.rect.topleft = pos
        self.number = number


        # 在方块中心显示数字
        font = pygame.font.Font(None, 27)
        text = font.render(str(number), 1, (79, 97 , 38))  # 数字颜色为深色
        textpos = text.get_rect(centerx=self.image.get_width()/2, centery=self.image.get_height()/2)
        self.image.blit(text, textpos)

    def clone(self):
        new_block = NumberBlock(self.number, self.rect.topleft)  # 创建一个新的NumberBlock实例
        return new_block

    def add_number(self, other_block):
        """将当前NumberBlock的数字值加上另一个NumberBlock对象的数字值"""
        self.number += other_block.number
        # 更新显示在屏幕上的数字块的数值
        font = pygame.font.Font(None, 27)
        text = font.render(str(self.number), 1, (79, 97, 38))  # 数字颜色为深色
        textpos = text.get_rect(centerx=self.image.get_width() / 2, centery=self.image.get_height() / 2)
        self.image.fill((173, 208, 124))  # 清除原有数字
        self.image.blit(text, textpos)

    def sub_number(self, other_block):
        """将当前NumberBlock的数字值加上另一个NumberBlock对象的数字值"""
        self.number -= other_block.number
        # 更新显示在屏幕上的数字块的数值
        font = pygame.font.Font(None, 27)
        text = font.render(str(self.number), 1, (79, 97, 38))  # 数字颜色为深色
        textpos = text.get_rect(centerx=self.image.get_width() / 2, centery=self.image.get_height() / 2)
        self.image.fill((173, 208, 124))  # 清除原有数字
        self.image.blit(text, textpos)

    def mul_number(self, other_block):
        """将当前NumberBlock的数字值加上另一个NumberBlock对象的数字值"""
        self.number *= other_block.number
        # 更新显示在屏幕上的数字块的数值
        font = pygame.font.Font(None, 27)
        text = font.render(str(self.number), 1, (79, 97, 38))  # 数字颜色为深色
        textpos = text.get_rect(centerx=self.image.get_width() / 2, centery=self.image.get_height() / 2)
        self.image.fill((173, 208, 124))  # 清除原有数字
        self.image.blit(text, textpos)

    def div_number(self, other_block):
        """将当前NumberBlock的数字值加上另一个NumberBlock对象的数字值"""
        self.number /= other_block.number
        # 更新显示在屏幕上的数字块的数值
        font = pygame.font.Font(None, 27)
        text = font.render(str(self.number), 1, (79, 97, 38))  # 数字颜色为深色
        textpos = text.get_rect(centerx=self.image.get_width() / 2, centery=self.image.get_height() / 2)
        self.image.fill((173, 208, 124))  # 清除原有数字
        self.image.blit(text, textpos)

    def squ_number(self):
        """将当前NumberBlock的数字值加上另一个NumberBlock对象的数字值"""
        self.number *= self.number
        # 更新显示在屏幕上的数字块的数值
        font = pygame.font.Font(None, 27)
        text = font.render(str(self.number), 1, (79, 97, 38))  # 数字颜色为深色
        textpos = text.get_rect(centerx=self.image.get_width() / 2, centery=self.image.get_height() / 2)
        self.image.fill((173, 208, 124))  # 清除原有数字
        self.image.blit(text, textpos)


blocks = pygame.sprite.Group()

def numbox(screen,result):

    if result == 0:
        numbers = [3, 1, 5, 3, 6, 2]  # 要显示的数字
    elif result == 1:
        numbers = [6, 7, 8,3, 4, 5]  # 要显示的数字
    elif result == 2:
        numbers = [3, 1, 6, 3, 6, 2]  # 要显示的数字
    elif result == 3:
        numbers = [1, 7, 5, 12, 8]  # 要显示的数字
    elif result == 4:
        numbers = [4, 3, 1, 2]  # 要显示的数字
    start_pos = (123, 320)  # 第一个方块的位置
    spacing = 40
    for i, number in enumerate(numbers):
        block1 = NumberBlock(number, (start_pos[0], start_pos[1] + i * spacing))
        blocks.add(block1)
    blocks.draw(screen)

def widgets(event,screen,play_button_image,cease,result):
    global flag,step,box,rk,story
    play_button_rect = play_button_image.get_rect()
    cease_image = cease.get_rect()
    play_button_rect.topleft = (330, 600)
    cease_image.topleft = (188, 605)
    #screen.blit(play_button_image, play_button_rect)
    if event.type == pygame.MOUSEBUTTONDOWN:
        if event.button == 1:

            if story <= 5:
                story += 1
            if play_button_rect.collidepoint(event.pos):
                flag = True
            for block in code_blocks:
                if block.rect.collidepoint(event.pos):
                    flag = False
                    step = 0
            for small_square in small_squares:
                if small_square.rect.collidepoint(event.pos):
                    flag = False
                    step = 0
            if cease_image.collidepoint(event.pos):
                blocks.empty()
                numbox(screen,result)
                flag = False
                box = 0
                step = 0
                rk = 0

def sign(screen,hand_image,boss_image,result):
    global story
    for i, block in enumerate(sorted_blocks):
        if block.rect.x > 900:
            font = pygame.font.Font("char.ttf", 18)
            text = font.render(str(i + 1), True, (130, 105, 88))
            pygame.draw.rect(screen, (173,146,127), (block.rect.x - 40, block.rect.y +3, 30, 30))
            screen.blit(text, (block.rect.x - 30, block.rect.y +3))
            # 代码序号
    if len(sorted_blocks) > 0 and sorted_blocks[step].rect.y - 30 >=100 :
        screen.blit(hand_image, (sorted_blocks[step].rect.x - 85, sorted_blocks[step].rect.y - 30))
        #代码指针
    boss_rect = boss_image.get_rect()
    boss_rect.topleft = (554, 93)



    if boss_rect.collidepoint(pygame.mouse.get_pos()):
        # 显示绿色对话框
        # 可以通过绘制矩形来实现，如：
        if result == 0:
            frame_color = (200, 181, 168)
            text_color = (58,53,48)
            dialog_text = "两数之差，算出负数即可通关"
            dialog_surface, dialog_pos = create_dialog_surface(dialog_text, (250, 200),frame_color,text_color)
            screen.blit(dialog_surface, dialog_pos)
        elif result == 1:
            frame_color = (200, 181, 168)
            text_color = (58,53,48)
            dialog_text = "将符合勾股定理的两边之和放于输出框，即可通关"
            dialog_surface, dialog_pos = create_dialog_surface(dialog_text, (250, 200),frame_color,text_color)
            screen.blit(dialog_surface, dialog_pos)
        elif result == 2:
            frame_color = (255, 255, 255)
            text_color = (0, 0, 0)
            dialog_text = "请实现六个方块依次完成加 乘 除 操作 ，即可通关"
            dialog_surface, dialog_pos = create_dialog_surface(dialog_text, (250, 200), frame_color, text_color)
            screen.blit(dialog_surface, dialog_pos)
        elif result == 3:
            frame_color = (255, 255, 255)
            text_color = (0, 0, 0)
            dialog_text = "将两个数中的所有整数包括他们本身，求和输出即可通关"
            dialog_surface, dialog_pos = create_dialog_surface(dialog_text, (250, 200), frame_color, text_color)
            screen.blit(dialog_surface, dialog_pos)
        elif result == 4:
            frame_color = (255, 255, 255)
            text_color = (0, 0, 0)
            dialog_text = "将圆周率约等于3 ，将所给半径地缘体积算出并输出即可通关"
            dialog_surface, dialog_pos = create_dialog_surface(dialog_text, (250, 200), frame_color, text_color)
            screen.blit(dialog_surface, dialog_pos)


    if result==0:
        if story == 0:
            dialog_text = "  ‘取箱’ 即拿起箱子"
        elif story == 1:
            dialog_text = "  ‘置案’ 即放下起箱子"
        elif story == 2:
            dialog_text = "  ‘誉至’ 即将箱子放到你想要的位置"
        elif story == 3:
            dialog_text = "  ‘相减’ 后面的数字就是你想减去数的位置"
        elif story == 4:
            dialog_text = "  ‘复始’ 就是重复做一件事情直到做完"
        if story < 5:
            dialog_surface, dialog_pos = create_dialog_surface(dialog_text, (250, 200),(156, 187, 93),(0,0,0))
            screen.blit(dialog_surface, dialog_pos)
    elif result==1:
        if story == 0:
            dialog_text = "中国古代的数学家们不仅很早就发现并应用勾股定理，而且很早就尝试对勾股定理作理论的证明。最早对勾股定理进行证明的"
        elif story == 1:
            dialog_text = "是三国时期吴国的数学家赵爽。赵爽创制了一幅“勾股圆方图”，用形数结合得到方法，给出了勾股定理的详细证明"
        elif story == 2:
            dialog_text = "赵爽用几何图形的截、割、拼、补来证明代数式之间的恒等关系，既具严密性，又具直观性，为中国古代以形证数、形数统一、代数和几何紧密结合、互不可分的独特风格树立了一个典范"
        elif story == 3:
            dialog_text = "以后的数学家大多继承了这一风格并且有发展，只是具体图形的分合移补略有不同而已"
        elif story == 4:
            dialog_text = "魏晋期间的数学家刘徽在证明勾股定理时也是用以形证数的方法，刘徽用了“出入相补法”，他把勾股为边的正方形上的某些区域剪下来（出），移到以弦为边的正方形的空白区域内（入），结果刚好填满，完全用图解法就解决了问题"
        if story < 5:
            dialog_surface, dialog_pos = create_dialog_surface(dialog_text, (250, 200),(156, 187, 93),(0,0,0))
            screen.blit(dialog_surface, dialog_pos)
    elif result==2:
        if story == 0:
            dialog_text = "在我国古代，人们很早就掌握了数的除法运算。最早使用是在先秦时期，或更早一些"
        elif story == 1:
            dialog_text = "形成于那个年代的《筭数书》中，关于除法的表示方式共有7类19种，涉及55条"
        elif story == 2:
            dialog_text = "自公元前春秋战国时代之前，我国出现了用“九九”表计算乘法以后，人们也总结了用口诀来计算除法的方法"
        elif story == 3:
            dialog_text = "《孙子算经》上说：“凡除之法，与乘正异"
        elif story == 4:
            dialog_text = "当时我国主要是用算筹和口诀来计算除法的"
        if story < 5:
            dialog_surface, dialog_pos = create_dialog_surface(dialog_text, (250, 200),(156, 187, 93),(0,0,0))
            screen.blit(dialog_surface, dialog_pos)
    elif result==3:
        if story == 0:
            dialog_text = "中国古代的《九章算术》等经典算术著作中已经涉及了一些数列及其求和方法。"
        elif story == 1:
            dialog_text = "例如，其中有关于等差数列的求和方法，如求和公式：[ S =(a_1 + a_n+1)(a_1 + a_n)/2 ]。"
        elif story == 2:
            dialog_text = "中国古代曾经使用垛积来解对数、矩阵甚至简单的微积分"
        elif story == 3:
            dialog_text = "朱世杰在《四元玉鉴》中记载了许多高阶等差数列的问题，"
        elif story == 4:
            dialog_text = "他列下了一串美丽的级数求和公式"
        if story < 5:
            dialog_surface, dialog_pos = create_dialog_surface(dialog_text, (250, 200),(156, 187, 93),(0,0,0))
            screen.blit(dialog_surface, dialog_pos)
    elif result==4:
        if story == 0:
            dialog_text = "《九章算术》的“少广”章的廿三及廿四两问中有所谓“开立圆术”，“立圆”的意思是“球体”，古称“丸”，而“开立圆术”即求已知体积的球体的直径的方法"
        elif story == 1:
            dialog_text = "其中廿四问为：“又有积一万六千四百四十八亿六千六百四十三万七千五百尺。问为立圆径几何？开立圆术曰：置积尺数，以十六乘之，九而一，所得开立方除之，即丸径。”"
        elif story == 2:
            dialog_text = "由于古代《周髀算经》对圆周率有“径一周三”的记载，故圆周率取3，就得到了“方”和“圆”的面积之比是4:3。"
        elif story == 3:
            dialog_text = "刘徽还是从正方体出发。他构思了一个看似奇葩，却有确实有效的物体，它叫做“牟合方盖”。"
        elif story == 4:
            dialog_text = "牟合方盖,祖暅原理,“方盖”和“球”的体积之比是4：π，此时可得到球的体积是4/3πr^3"
        if story < 5:
            dialog_surface, dialog_pos = create_dialog_surface(dialog_text, (250, 200),(156, 187, 93),(0,0,0))
            screen.blit(dialog_surface, dialog_pos)

def end_processing(character,screen,next1):
    global flag, step, box, rk, story,outbox,directionf#number
    flag = False
    character.move_to((300, 410))
    box = 0
    step = 0
    rk = 0

    dialog_text = "你已成功学会"
    dialog_surface, dialog_pos = create_dialog_surface(dialog_text, (250, 200),(156,187,93),(64,87,9))
    screen.blit(dialog_surface, dialog_pos)
    for event in pygame.event.get():
        next_button_rect = next1.get_rect()
        next_button_rect.topleft = (400, 200)
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                if next_button_rect.collidepoint(event.pos):
                    return True

def pd():
    global flag
    return flag




