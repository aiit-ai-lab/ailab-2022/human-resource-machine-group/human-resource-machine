import threading
import time


# 定义在子线程中运行的函数
def loop_in_thread():
    i = 100
    while i < 110:
        print("Thread loop:", i)
        time.sleep(1)
        i += 1


def main():
    # 创建并启动子线程
    t = threading.Thread(target=loop_in_thread)
    t.start()

    # 在主线程中执行的循环
    i = 0
    while i < 10:
        print("Main loop:", i)
        time.sleep(1)
        i += 1


# 运行主函数
if __name__ == '__main__':
    main()