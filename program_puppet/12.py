import threading
import random
import time

# 用于存储随机数组的全局变量
random_array = []

# 定义第一个功能的函数，用于生成随机数组
def generate_random_array():
    global random_array
    while True:
        if random_array == []:
            random_array = [random.randint(1, 100) for _ in range(10)]
            print("Generated random array:", random_array)
        #time.sleep(1)  # 等待1秒

# 定义第二个功能的函数，用于打印随机数组
def print_random_array():
    global random_array
    while True:
        if random_array:  # 检查是否有随机数组
            print("Received random array in second thread:", random_array)
            random_array = []  # 清空随机数组
        #time.sleep(1)  # 等待1秒

# 创建线程对象，分别指定要运行的函数
thread1 = threading.Thread(target=generate_random_array)
thread2 = threading.Thread(target=print_random_array)

# 启动线程
thread1.start()
thread2.start()



# 主线程等待一段时间后结束
#time.sleep(10)  # 等待10秒
print("Main thread is ending")
