import pygame,sys
expand_width = 0
back_x = 0

class Button(object):
    # 构造函数
    def __init__(self, buttonUpImage, buttonDownImage, pos):
        # 按钮未按下的图片样式
        self.buttonUp = pygame.image.load(buttonUpImage)
        # 按钮按下的图片样式
        self.buttonDown = pygame.image.load(buttonDownImage)
        # 按钮在窗口中的位置
        self.pos = pos

    # 检查鼠标是否在按钮图片范围内
    def inButtonRange(self):
        # 获取鼠标的位置
        mouseX, mouseY = pygame.mouse.get_pos()
        x, y = self.pos
        w, h = self.buttonUp.get_size()
        inX = x - w / 2 < mouseX < x + w / 2
        inY = y - h / 2 < mouseY < y + h / 2
        return inX and inY

    # 在窗口中显示按钮
    def show(self, screen):
        w, h = self.buttonUp.get_size()
        x, y = self.pos
        # 根据鼠标位置变换样式
        if self.inButtonRange():
            screen.blit(self.buttonDown, (x - w / 2, y - h / 2))
        else:
            screen.blit(self.buttonUp, (x - w / 2, y - h / 2))

def showGameInterface(screen, back_interface, interface, game_1, game_2, game_3, game_4, juanzhou1, juanzhou2):
    global expand_width, back_x
    expand_speed = 10  # 根据需要调整展开速度
    max_expand_width = 565  # 根据需要调整最大展开宽度
    screen.fill((0, 0, 0))
    screen.blit(back_interface, (0, 0))
    screen.blit(interface, (0, 45))
    # 如果展开完成，向两边展开
    # 如果展开完成，向两边展开
    if expand_width < max_expand_width:
        expand_width += expand_speed
        screen.fill((78, 14, 14), (0, 0, 600 - expand_width, 675))  # Left gray background
        screen.fill((78, 14, 14), (600 + expand_width, 0, 1200, 675))  # Right white background
        screen.blit(juanzhou2, (600 - expand_width - juanzhou2.get_width(), 0))
        screen.blit(juanzhou1, (600 + expand_width, 0))
    else:
        screen.blit(juanzhou2, (0, 0))  # 如果展开完成，直接显示juan2
        screen.blit(juanzhou1, (1200 - juanzhou2.get_width(), 0))  # 如果展开完成，直接显示juan1
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if game_1.inButtonRange():
                    return 1
                elif game_2.inButtonRange():
                    return 2
                elif game_3.inButtonRange():
                    return 3
                elif game_4.inButtonRange():
                    return 4
        # screen.blit(interface, (0, 0))
        game_1.show(screen)
        game_2.show(screen)
        game_3.show(screen)
        game_4.show(screen)
    pygame.display.flip()
    return 8



