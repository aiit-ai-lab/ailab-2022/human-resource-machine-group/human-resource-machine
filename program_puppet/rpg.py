import pygame
import cv2
import numpy as np


def create_dialog_surface(text, pos,frame_color,text_color):

    font = pygame.font.Font("talk.ttf", 20)  # 设置字体和大小
    dialog_text_lines = []
    temp_line = ''
    max_width = 300
    for char in text:
        temp_line += char
        if font.size(temp_line)[0] > max_width:
            dialog_text_lines.append(temp_line[:-1])
            temp_line = char
    dialog_text_lines.append(temp_line)
    text_height = font.get_linesize() * len(dialog_text_lines)
    dialog_width = 320
    dialog_height = text_height + 20
    dialog_surface = pygame.Surface((dialog_width, dialog_height), pygame.SRCALPHA)
    dialog_surface.set_alpha(200)
    pygame.draw.rect(dialog_surface, frame_color, (0, 0, dialog_width, dialog_height), border_radius=10)
    y = 10
    for line in dialog_text_lines:
        dialog_text = font.render(line, True, text_color)
        dialog_surface.blit(dialog_text, (10, y))
        y += font.get_linesize()
    return dialog_surface, (pos[0], pos[1] - dialog_height)


class Character:
    def __init__(self, image_path, initial_position):
        # 加载角色图像
        char_image = pygame.image.load(image_path)
        char_width, char_height = char_image.get_size()
        self.char_images = [
            char_image.subsurface(j * (char_width // 3), i * ((char_height // 4)), char_width // 3, char_height // 4)
            for i in range(4) for j in range(3)]
        # 获取图像的矩形对象
        self.rect = self.char_images[0].get_rect()
        # 设置角色的初始位置
        self.rect.topleft = initial_position
        # 设置角色的移动速度
        self.speed = 2
        self.current_image = 0
        self.direction = None


    def move(self,output_image):
        global bg_x ,bg_y
        if self.direction == 'left' and all(
                output_image[self.rect.y + y+14-bg_y, self.rect.x+14 - bg_x] == 0 for y in range(1,4)):
            if self.rect.x > 150 or (bg_x >= 0 and self.rect.x >= 0):
                self.rect.x -= self.speed
            elif bg_x <= -1:
                bg_x += self.speed
            self.current_image = (self.current_image + 1) % 3 + 3  # 循环切换左走的动作
        elif self.direction == 'right' and all(
                output_image[self.rect.y + y+14-bg_y, self.rect.x + 18 - bg_x] == 0 for y in range(1,4)):
            if self.rect.x < 450 or (bg_x <= -400 and self.rect.x <= 568):
                self.rect.x += self.speed
            elif bg_x >= -399:
                bg_x -= self.speed
            self.current_image = (self.current_image + 1) % 3 + 6  # 循环切换右走的动作
        elif self.direction == 'up' and all(
                output_image[self.rect.y+14 - bg_y, self.rect.x + x + 14 - bg_x] == 0 for x in range(1,4)):
            if self.rect.y >= 150 or (bg_y >= 0 and self.rect.y >= 0):
                self.rect.y -= self.speed
            elif bg_y <= -1:
                bg_y += self.speed
            self.current_image = (self.current_image + 1) % 3 + 9    # 循环切换上走的动作
        elif self.direction == 'down'and all(
                output_image[self.rect.y+18 - bg_y, self.rect.x + x + 14 - bg_x] == 0 for x in range(1,4)):
            if self.rect.y <= 450 or (bg_y <= -400 and self.rect.y <= 568):
                self.rect.y += self.speed
            elif bg_y >= -399:
                bg_y -= self.speed
            self.current_image = (self.current_image + 1) % 3   # 循环切换下走的动作
    def draw(self, screen):
        # 在屏幕上绘制角色
        screen.blit(self.char_images[self.current_image], self.rect)


def rpg_main():
    global bg_x,bg_y
    pygame.init()
    screen_width = 600
    screen_height = 600

    clock = pygame.time.Clock()
    background = pygame.image.load('openbig4.jpg')
    background1 = pygame.image.load('openbig5.jpg')
    background2 = pygame.image.load('openbig3.jpg')
    background3 = pygame.image.load('openbig1.jpg')
    background4 = pygame.image.load('openbig2.jpg')
    bg_width, bg_height = background.get_size()
    bg_x = (screen_width - bg_width) // 2
    bg_y = (screen_height - bg_height) // 2
    screen = pygame.display.set_mode((screen_width, screen_height))

    try:
        output_image = np.load('edge_big4.npy')
        output_image1 = np.load('edge_big5.npy')
        output_image2 = np.load('edge_big3.npy')
        output_image3 = np.load('edge_big1.npy')
        output_image4 = np.load('edge_big2.npy')


        print("发现已保存的数组文件 'output_image.npy'，将其读入 output_image1 数组中。")
    except FileNotFoundError:
        image = cv2.imread('edge_big2.jpg', cv2.IMREAD_GRAYSCALE)
        edges = cv2.Canny(image, 100, 200)
        output_image = np.zeros_like(image)
        np.save('edge_big9.npy', output_image)
        print("未找到已保存的数组文件 'output_image.npy'，将创建新数组 output_image1。")
        for i in range(edges.shape[0]):
            for j in range(edges.shape[1]):
                if edges[i, j] > 0:
                    output_image[i, j] = 255
        cv2.imwrite('output_edge_points.jpg', output_image)
    character = Character("char0.png", (400, 300))

    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    character.direction = 'left'
                elif event.key == pygame.K_RIGHT:
                    character.direction = 'right'
                elif event.key == pygame.K_UP:
                    character.direction = 'up'
                elif event.key == pygame.K_DOWN:
                    character.direction = 'down'
            if event.type == pygame.KEYUP:
                character.direction = None



        screen.blit(background, (bg_x, bg_y))




        #________________对话框————————————————
        frame_color = (200, 181, 168)
        text_color = (58, 53, 48)
        dialog_text = "请实现六个方块依次完成加 乘 除 操作 ，即可通关"
        dialog_surface, dialog_pos = create_dialog_surface(dialog_text, (250, 200), frame_color, text_color)
        screen.blit(dialog_surface, dialog_pos)
        #————————————————对话框——————————————————
        character.move(output_image)
        character.draw(screen)
        clock.tick(60)
        pygame.display.flip()

if __name__ == '__main__':
    rpg_main()