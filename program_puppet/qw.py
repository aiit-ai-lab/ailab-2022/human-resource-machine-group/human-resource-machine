import pygame
import threading

# 初始化pygame
pygame.init()

# 设置窗口尺寸
win_width = 800
win_height = 600
win = pygame.display.set_mode((win_width, win_height))

# 定义颜色
RED = (255, 0, 0)
BLUE = (0, 0, 255)

char_img_red = pygame.image.load("BoxIcon.png")
char_img_blue = pygame.image.load("hand.png")



# 红色图片的初始位置和拖动状态
red_image_x = 200
red_image_y = 300
red_dragging = False
red_offset_x = 0
red_offset_y = 0

# 蓝色图片的初始位置和拖动状态
blue_image_x = 400
blue_image_y = 300
blue_dragging = False
blue_offset_x = 0
blue_offset_y = 0

# 绘制红色圆形的函数
def draw_red_circle():
    running = True
    while running:
        win.blit(char_img_red, (red_image_x, red_image_y))
        pygame.display.update(red_image_x, red_image_y, char_img_red.get_width(), char_img_red.get_height())
        pygame.display.update()

# 绘制蓝色矩形的函数
def draw_blue_rect():
    running = True
    while running:
        win.blit(char_img_blue, (blue_image_x, blue_image_y))
        pygame.display.update(blue_image_x, blue_image_y, char_img_blue.get_width(), char_img_blue.get_height())
        pygame.display.update()

# 创建并启动线程
red_circle_thread = threading.Thread(target=draw_red_circle)
blue_rect_thread = threading.Thread(target=draw_blue_rect)
red_circle_thread.start()
blue_rect_thread.start()

# 游戏主循环

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:  # 左键按下
                mouse_x, mouse_y = event.pos
                # 检查鼠标是否在红色图像上
                if red_image_x < mouse_x < red_image_x + char_img_red.get_width() and red_image_y < mouse_y < red_image_y + char_img_red.get_height():
                    red_dragging = True
                    red_offset_x = mouse_x - red_image_x
                    red_offset_y = mouse_y - red_image_y
                # 检查鼠标是否在蓝色图像上
                elif blue_image_x < mouse_x < blue_image_x + char_img_blue.get_width() and blue_image_y < mouse_y < blue_image_y + char_img_blue.get_height():
                    blue_dragging = True
                    blue_offset_x = mouse_x - blue_image_x
                    blue_offset_y = mouse_y - blue_image_y
        elif event.type == pygame.MOUSEMOTION:
            if red_dragging:
                mouse_x, mouse_y = event.pos
                red_image_x = mouse_x - red_offset_x
                red_image_y = mouse_y - red_offset_y
                pygame.display.update(red_image_x, red_image_y, char_img_red.get_width(), char_img_red.get_height())
            elif blue_dragging:
                mouse_x, mouse_y = event.pos
                blue_image_x = mouse_x - blue_offset_x
                blue_image_y = mouse_y - blue_offset_y
                pygame.display.update(blue_image_x, blue_image_y, char_img_blue.get_width(), char_img_blue.get_height())
        elif event.type == pygame.MOUSEBUTTONUP:
            red_dragging = False
            blue_dragging = False
    win.fill((0, 0, 0))

# 退出pygame
pygame.quit()