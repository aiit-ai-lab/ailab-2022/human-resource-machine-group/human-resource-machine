from game_begins2 import *
import threading
import numpy as np

dragging_block = None
offset_x = 0
offset_y = 0
sorted_blocks =[]
gunblocks = []
lnitial_value = 130

class SmallSquare(pygame.sprite.Sprite):
    def __init__(self, position,text):
        super().__init__()
        self.image = pygame.Surface((35, 35))
        self.text = text
        if self.text == " 相 和" or self.text == " 乘 以" or self.text == " 除 以" or self.text == " 相 减" :
            self.image.fill((188, 138, 105))
        else:
            self.image.fill((196, 107, 87))
        self.rect = self.image.get_rect()
        self.rect.topleft = position
        self.is_dragging = False
        self.value = 0

        font = pygame.font.Font(None, 36)
        if self.text == " 相 和" or self.text == " 乘 以" or self.text == " 除 以" or self.text == " 相 减" :
            value_text = font.render(str(self.value), True, (81, 53, 23))
        else:
            value_text = font.render(str(self.value), True, (101, 33, 17))
        # 调整坐标，使数字居中显示在方块内部
        text_rect = value_text.get_rect(center=self.image.get_rect().center)
        self.image.blit(value_text, text_rect.topleft)

    def update(self):
        if self.is_dragging:
            self.rect.center = pygame.mouse.get_pos()
    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:  # 鼠标左键按下
                if self.rect.collidepoint(event.pos):
                    self.is_dragging = True
        elif event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:  # 鼠标左键释放
                if self.rect.collidepoint(event.pos):
                    self.is_dragging = False
                    # 检查释放位置是否在指定区域
                    if 300 <= event.pos[0] <= 360 and 340 <= event.pos[1] <= 400:
                        self.value = 1
                    elif 365 <= event.pos[0] <= 425 and 340 <= event.pos[1] <= 400:
                        self.value = 2
                    elif 430 <= event.pos[0] <= 490 and 340 <= event.pos[1] <= 400:
                        self.value = 3
                    # # 更新小方块上显示的数字
                    font = pygame.font.Font(None, 36)
                    if self.text == " 相 和" or self.text == " 乘 以" or self.text == " 除 以":
                        value_text = font.render(str(self.value), True, (81, 53, 23))
                    else:
                        value_text = font.render(str(self.value), True, (101, 33, 17))
                    text_rect = value_text.get_rect(center=self.image.get_rect().center)
                    if self.text == " 相 和" or self.text == " 乘 以" or self.text == " 除 以" or self.text == " 相 减" or self.text == " 平 方":
                        self.image.fill((188, 138, 105))
                    else:
                        self.image.fill((196, 107, 87))
                    self.image.blit(value_text, text_rect.topleft)





class CodeBlock(pygame.sprite.Sprite):
    def __init__(self, text, pos):
        super().__init__()
        self.image = pygame.Surface((100, 35))
        if text == " 抄 誉":
            self.image.fill((196, 107, 87))
        elif text == " 复 始" or text == " 疑 零":
            self.image.fill((142, 139, 184))
        elif text == "      ":
            self.image.fill((142, 139, 184))
        elif text == " 誉 至":
            self.image.fill((196, 107, 87))
        elif text == " 相 和" or text == " 乘 以" or text == " 除 以" or text == " 相 减" or text == " 平 方":
            self.image.fill((188, 138, 105))
        else:
            self.image.fill((156, 187, 93))
        self.rect = self.image.get_rect()
        self.rect.topleft = pos
        self.original_position = pos
        self.text = text
        self.has_sub_block = False
        self.linked_block = None
        self.small_square =None


    def upui(self):
        font = pygame.font.Font("char3.ttf", 25)
        if self.text == " 抄 誉":
            text = font.render(self.text, False, (101, 33, 17))
        elif self.text == " 复 始" or self.text == " 疑 零":
            text = font.render(self.text, False, (50, 54, 92))
        elif self.text == " 誉 至":
            text = font.render(self.text, False, (101, 33, 17))
        elif self.text == " 相 和" or self.text == " 乘 以" or self.text == " 除 以" or self.text == " 相 减" or self.text == " 平 方":
            text = font.render(self.text, False, (81, 53, 23))
        else:
            text = font.render(self.text, False, (64, 87, 9))
        self.image.blit(text, (5, 4))

        if self.rect.right >= 900 and self.small_square is not None:
            self.small_square.rect.topleft = (self.rect.right + 6, self.rect.top)


    def updata(self,result):
        print('h')
        global sorted_blocks,res
        if result == 0:
            if self.text == " 取 箱":
                code_y = 1
            elif self.text == " 置 案":
                code_y = 2
            elif self.text == " 誉 至":
                code_y = 3
            elif self.text == " 相 减":
                code_y = 4
            elif self.text == " 复 始":
                code_y = 5
            elif self.text == "      ":
                code_y = -4
        else:
            if self.text == " 取 箱":
                code_y = 1
            elif self.text == " 抄 誉":
                code_y = 3
            elif self.text == " 置 案":
                code_y = 2
            elif self.text == " 复 始":
                code_y = 5
            elif self.text == " 誉 至":
                code_y = 4
            elif self.text == " 相 和":
                code_y = 6
            elif self.text == " 乘 以":
                code_y = 7
            elif self.text == " 除 以":
                code_y = 8
            elif self.text == " 疑 零":
                code_y = 9
            elif self.text == " 相 减":
                code_y = 10
            elif self.text == " 平 方":
                code_y = 11
            elif self.text == "      ":
                code_y = -4

        if self.rect.right >= 900 :
            if self.text==" 复 始" and self.has_sub_block == False:
                x,y=self.rect.topleft
                new_sub_block = CodeBlock("      ", (x,y+40))  # 位于下一个位置
                code_blocks.append(new_sub_block)
                self.linked_block = new_sub_block
                self.has_sub_block = True

            elif self.text == " 誉 至" and not self.has_sub_block:
                # 创建小方块
                self.small_square = SmallSquare((self.rect.right + 6, self.rect.top)," 誉 至")
                small_squares.append(self.small_square)
                self.has_sub_block = True

            elif self.text == " 抄 誉" and not self.has_sub_block:
                # 创建小方块
                self.small_square = SmallSquare((self.rect.right + 6, self.rect.top)," 抄 誉")
                small_squares.append(self.small_square)
                self.has_sub_block = True

            elif self.text == " 相 和" and not self.has_sub_block:
                # 创建小方块
                self.small_square = SmallSquare((self.rect.right + 6, self.rect.top)," 相 和")
                small_squares.append(self.small_square)
                self.has_sub_block = True

            elif self.text == " 乘 以" and not self.has_sub_block:
                # 创建小方块
                self.small_square = SmallSquare((self.rect.right + 6, self.rect.top)," 乘 以")
                small_squares.append(self.small_square)
                self.has_sub_block = True

            elif self.text == " 除 以" and not self.has_sub_block:
                # 创建小方块
                self.small_square = SmallSquare((self.rect.right + 6, self.rect.top)," 除 以")
                small_squares.append(self.small_square)
                self.has_sub_block = True

            elif self.text == " 疑 零" and not self.has_sub_block:
                x, y = self.rect.topleft
                new_sub_block = CodeBlock("      ", (x, y + 40))  # 位于下一个位置
                code_blocks.append(new_sub_block)
                self.linked_block = new_sub_block
                self.has_sub_block = True

            elif self.text == " 相 减" and not self.has_sub_block:
                # 创建小方块
                self.small_square = SmallSquare((self.rect.right + 6, self.rect.top)," 相 减")
                small_squares.append(self.small_square)
                self.has_sub_block = True


            new_block = CodeBlock(self.text, (750, 90+40 * code_y))
            code_blocks.append(new_block)


        else:
            self.rect.topleft = self.original_position
            if self.linked_block is not None:
                code_blocks.remove(self.linked_block)
                dragging_block.linked_block = None
                dragging_block.has_sub_block = False
            if self.small_square is not None:
                dragging_block.has_sub_block = False
                small_squares.remove(self.small_square)
                dragging_block.small_square = None


    def handle_event(self, event, code_blocks,result):
        global dragging_block,offset_x,offset_y,sorted_blocks,gunblocks,lnitial_value
        goal_p = 900
        goal_blocks = []
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:  # 左键按下，尝试拖拽代码块
                for block in code_blocks:
                    if block.rect.collidepoint(event.pos):
                        dragging_block = block
                        offset_x = event.pos[0] - dragging_block.rect.x
                        offset_y = event.pos[1] - dragging_block.rect.y
                        break
            elif event.button == 4 and len(gunblocks)>=6:
                if gunblocks[-1].rect.top > 380 :
                    lnitial_value -=5
                    for block in gunblocks:
                        block.rect.y = block.rect.y - 5  # 相对移动gunblocks
                        print(f"gunblock {block.text} 的位置是 {block.rect.y}")
                    print('-----------------------------------------------')
            elif event.button == 5 and len(gunblocks)>=6:  # 滚轮向下滚动
                if gunblocks[1].rect.top < 420:
                    lnitial_value += 5
                    for block in gunblocks:
                        block.rect.y = block.rect.y + 5 # 相对移动gunblocks
                        print(f"gunblock {block.text} 的位置是 {block.rect.y}")
                    print('-----------------------------------------------')
        elif event.type == pygame.MOUSEBUTTONUP:
            if dragging_block is not None:  # 拖拽状态下的鼠标移动事件
                dragging_block.updata(result)
                if dragging_block.rect.right >= goal_p:  # 如果代码块拖到屏幕最右侧
                    for block in code_blocks:
                        if block.rect.right >= goal_p:
                            goal_blocks.append(block)
                    sorted_blocks = sorted(goal_blocks, key=lambda block: block.rect.top)
                    gunblocks = sorted_blocks
                    for i, block1 in enumerate(sorted_blocks):
                        block1.rect.topleft = (940, lnitial_value + i * 45)
                        print(f"代码块 {block1.text} 的顺序是 {i + 1}")
                    for i, blockx in enumerate(sorted_blocks):
                        if blockx.linked_block is not None:
                            for j, blocko in enumerate(sorted_blocks):
                                if blocko == blockx.linked_block:
                                    blockx.jump = j
                    print('------------------------------------------------')
                else :
                    for block in code_blocks:
                        if block.rect.right >= goal_p:
                            goal_blocks.append(block)
                    sorted_blocks = sorted(goal_blocks, key=lambda block: block.rect.top)
                    gunblocks = sorted_blocks
                    for i, block1 in enumerate(sorted_blocks):
                        block1.rect.topleft = (940, lnitial_value + i * 45)

                dragging_block = None
        elif event.type == pygame.MOUSEMOTION:
            if dragging_block is not None:  # 如果有代码块正在被拖拽
                dragging_block.rect.x = event.pos[0] - offset_x
                dragging_block.rect.y = event.pos[1] - offset_y

def calculate_curve_points(screen,block,module_size=50):
     # 如果有连接的块
    module1 = block.rect.topleft
    module2 = block.linked_block.rect.topleft

    # 增加起始点和结束点的 x 坐标
    start_x = module1[0] + block.rect.width
    end_x = module2[0] + block.linked_block.rect.width

    # 控制点可以位于两个模块的中点，并且y轴上稍微偏离
    control_point1 = (start_x + int(abs(module1[1] - module2[1]) / 5 + 10),
                      (module1[1] + module2[1] + module_size) // 2)

    t = np.linspace(0, 1, 50000)
    x_curve = (1 - t) ** 2 * start_x + 2 * (1 - t) * t * control_point1[0] + t ** 2 * end_x
    y_curve = (1 - t) ** 2 * (module1[1] + module_size // 2) + 2 * (1 - t) * t * control_point1[
        1] + t ** 2 * (module2[1] + module_size // 2)

    curve_points = list(zip(x_curve, y_curve))
    pygame.draw.lines(screen, (142, 139, 184), False, curve_points, 3)



code_blocks = []
small_squares = []

def commands():
    global sorted_blocks
    return sorted_blocks


def module_init(result):
    if result == 0:
        block = CodeBlock(f" 取 箱", (750, 90 + 40 * 1))
        code_blocks.append(block)
        block = CodeBlock(f" 置 案", (750, 90 + 40 * 2))
        code_blocks.append(block)
        block = CodeBlock(f" 誉 至", (750, 90 + 40 * 3))
        code_blocks.append(block)
        block = CodeBlock(f" 相 减", (750, 90 + 40 * 4))
        code_blocks.append(block)
        block = CodeBlock(f" 复 始", (750, 90 + 40 * 5))
        code_blocks.append(block)
    else:
        block = CodeBlock(f" 取 箱", (750, 90 + 40 * 1))
        code_blocks.append(block)
        block = CodeBlock(f" 置 案", (750, 90 + 40 * 2))
        code_blocks.append(block)
        block = CodeBlock(f" 抄 誉", (750, 90 + 40 * 3))
        code_blocks.append(block)
        block = CodeBlock(f" 誉 至", (750, 90 + 40 * 4))
        code_blocks.append(block)
        block = CodeBlock(f" 相 和", (750, 90 + 40 * 6))
        code_blocks.append(block)
        block = CodeBlock(f" 复 始", (750, 90 + 40 * 5))
        code_blocks.append(block)
        block = CodeBlock(f" 乘 以", (750, 90 + 40 * 7))
        code_blocks.append(block)
        block = CodeBlock(f" 除 以", (750, 90 + 40 * 8))
        code_blocks.append(block)
        block = CodeBlock(f" 疑 零", (750, 90 + 40 * 9))
        code_blocks.append(block)
        block = CodeBlock(f" 相 减", (750, 90 + 40 * 10))
        code_blocks.append(block)
        block = CodeBlock(f" 平 方", (750, 90 + 40 * 11))
        code_blocks.append(block)

