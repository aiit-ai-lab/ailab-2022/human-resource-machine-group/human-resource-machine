import pygame,sys
expand_width = 0
back_x = 0
class Button(object):
    # 构造函数
    def __init__(self, buttonUpImage, buttonDownImage, pos):
        # 按钮未按下的图片样式
        self.buttonUp = pygame.image.load(buttonUpImage)
        # 按钮按下的图片样式
        self.buttonDown = pygame.image.load(buttonDownImage)
        # 按钮在窗口中的位置
        self.pos = pos

    # 检查鼠标是否在按钮图片范围内
    def inButtonRange(self):
        # 获取鼠标的位置
        mouseX, mouseY = pygame.mouse.get_pos()
        x, y = self.pos
        w, h = self.buttonUp.get_size()
        inX = x - w / 2 < mouseX < x + w / 2
        inY = y - h / 2 < mouseY < y + h / 2
        return inX and inY

    # 在窗口中显示按钮
    def show(self, screen):
        w, h = self.buttonUp.get_size()
        x, y = self.pos
        # 根据鼠标位置变换样式
        if self.inButtonRange():
            screen.blit(self.buttonDown, (x - w / 2, y - h / 2))
        else:
            screen.blit(self.buttonUp, (x - w / 2, y - h / 2))

def showGameInterface(screen, interface, startGame, gameTips,juan1,juan2,backImage):
    global expand_width,back_x

    expand_speed = 3  # 根据需要调整展开速度
    max_expand_width = 565  # 根据需要调整最大展开宽度




    screen.fill((0,0,0))
    #screen.fill((46, 27, 10),(0,35,1200,675))
    screen.blit(interface, (0,55))



    # 如果展开完成，向两边展开
    # 如果展开完成，向两边展开
    if expand_width < max_expand_width:
        expand_width += expand_speed
        screen.fill((78, 14, 14), (0, 0, 600 - expand_width, 675))  # Left gray background
        screen.fill((78, 14, 14), (600 + expand_width, 0, 1200, 675))  # Right white background
        screen.blit(juan2, (600 - expand_width - juan2.get_width(), 0))
        screen.blit(juan1, (600 + expand_width, 0))
    else:
        screen.blit(juan2, (0, 0))  # 如果展开完成，直接显示juan2
        screen.blit(juan1, (1200 - juan2.get_width(), 0))  # 如果展开完成，直接显示juan1
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if startGame.inButtonRange():
                    return 1
                elif gameTips.inButtonRange():
                    return 2
        #startGame.show(screen)
        #gameTips.show(screen)




    pygame.display.flip()
    return 8

def main():
    # 初始化pygame
    pygame.init()
    try:

        # 设置游戏图标
        gameicon = pygame.image.load("BoxIcon.png")
        # # 展示游戏图标
        pygame.display.set_icon(gameicon)

        pygame.display.set_caption('I Love Sokoban ~')
        screen = pygame.display.set_mode((1200, 675))
        button1 = Button('GameStartUp.png', 'GameStartDown.png', (200, 300))
        # “第一关”按钮
        buttonc1 = Button('Chapter1Up.png', 'Chapter1Down.png', (200, 175))
        juan1 = pygame.image.load("juan1.png")
        juan2 = pygame.image.load("juan2.png")
        back_image = pygame.image.load("back.jpg")
        # “第二关”按钮
        # 主界面背景图片
        interface = pygame.image.load("back1.png")

    except pygame.error as msg:
        raise (SystemExit(msg))


    while True:
        result = showGameInterface(screen, interface, button1, buttonc1,juan1,juan2,back_image)
        if result == 0:  # 如果返回0，退出游戏
            break
        elif result == 1:  # 如果返回1，执行开始游戏的逻辑
            print("%%%%%%%%%%%%%%%%%%%")
        elif result == 2:  # 如果返回2，执行游戏说明的逻辑
            print("??????????????????????????????????????????????????????")



if __name__ == '__main__':
    main()