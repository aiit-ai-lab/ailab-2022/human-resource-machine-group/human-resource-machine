import pygame

# 初始化 pygame
pygame.init()

# 定义窗口大小和方块大小
WINDOW_WIDTH = 800
WINDOW_HEIGHT = 200
BLOCK_SIZE = 50

# 定义颜色
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
RED = (255, 0, 0)

# 创建窗口
window = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))

# 加载字体
font = pygame.font.Font(None, 30)

# 初始化方块列表
blocks = [2, 3,4,5]

# 游戏主循环
running = True
while running:
    # 处理事件
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_PLUS or event.key == pygame.K_KP_PLUS:
                if len(blocks) > 1:
                    result = blocks[0] + blocks[1]
                    blocks = [result] + blocks[2:]
            elif event.key == pygame.K_MINUS or event.key == pygame.K_KP_MINUS:
                if len(blocks) > 1:
                    result = blocks[0] - blocks[1]
                    blocks = [result] + blocks[2:]
            elif event.key == pygame.K_ASTERISK:
                if len(blocks) > 1:
                    result = blocks[0] * blocks[1]
                    blocks = [result] + blocks[2:]
            elif event.key == pygame.K_SLASH:
                if len(blocks) > 1:
                    result = blocks[0] / blocks[1]
                    blocks = [result] + blocks[2:]

    # 清空窗口
    window.fill(WHITE)

    # 绘制方块和数字
    for i, num in enumerate(blocks):
        x = i * BLOCK_SIZE
        y = (WINDOW_HEIGHT - BLOCK_SIZE) // 2
        rect = pygame.Rect(x, y, BLOCK_SIZE, BLOCK_SIZE)
        pygame.draw.rect(window, BLUE, rect)
        text = font.render(str(num), True, WHITE)
        text_rect = text.get_rect(center=rect.center)
        window.blit(text, text_rect)

    # 更新窗口显示
    pygame.display.flip()

# 退出游戏
pygame.quit()