import cv2
import numpy as np

# 读取原始图片
image = cv2.imread('edge_big1gai.jpg', cv2.IMREAD_GRAYSCALE)

# 使用Canny算法进行边缘检测
edges = cv2.Canny(image, 100, 200)

# 创建与原图像大小相同的空白图像
output_image = np.zeros_like(edges)

# 找到边缘像素并在新图像中标记为黑色
for i in range(edges.shape[0]):
    for j in range(edges.shape[1]):
        if edges[i, j] > 0:
            output_image[i, j] = 255

cv2.imwrite('edge_big1gai.jpg', output_image)
# 判断是否已经存在存储数组的文件，若存在则读入该数组
try:
    output_image1 = np.load('output_image.npy')
    print("发现已保存的数组文件 'output_image.npy'，将其读入 output_image1 数组中。")
except FileNotFoundError:
    output_image1 = np.zeros_like(edges)
    print("未找到已保存的数组文件 'output_image.npy'，将创建新数组 output_image1。")

# 保存当前数组
np.save('edge_big1.npy', output_image)

print("边缘点图像已保存到 output_edge_points.jpg 文件中。")
