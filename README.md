# Human Resource Machine

## 关于程序木偶

本游戏创意源于Human Resource Machine，及采用人偶行为模拟数据处理过程

游戏底层采用python开发

**开发环境**      python3.12 +pygame

## 项目介绍

(1) 由游戏中的程序框图转化成Tiderip解释器可以识别的编程语言

(2) 程序框图的遍历，以链表储存

(3) Tiderip解释器处理编程语言后输出的处理结果，控制人物动作行为，反映算法过程

(4) 导入数据结构中的基本算法

(5) 由人物动作解释数据结构基础算法 ，并可自由更改算法，对动态数据进行动态演示

## 文件大纲

1.拥有五块游戏地图

2.可与NPC经行对话

3.找到卷轴可跳转游戏页面

4.可执行代码块中的所有动作

***

# 项目创新点

(1) 简化编程过程中复杂的语法，以及对设备的强依赖性

(2) 通过动态的程序框图自由调节，使代码逻辑更加清晰

(3) 突破以往固有游戏规则对于游戏代码展示能力的束缚，引入更多的数据结构内容，增强对数据的动态处理能力

(4) 将算法学习融入编程中并自由改动，可更高效的发现算法错误，提高算法能力

(5) 用户代码逻辑错误经行智能提示，辅助玩家经行游戏通关

## 预期效果

### 进入页面

![](https://gitlab.com/aiit-ai-lab/ailab-2022/human-resource-machine-group/human-resource-machine/-/raw/main/program_puppet/%E6%BC%94%E7%A4%BA/%7BE12D8D47-EC5F-4dec-AC8A-91CB7E9E3D05%7D.png?ref_type=heads)

### 游戏地图

![](https://gitlab.com/aiit-ai-lab/ailab-2022/human-resource-machine-group/human-resource-machine/-/raw/main/program_puppet/%E6%BC%94%E7%A4%BA/%7BF4024229-2A9D-4d78-97E9-B9F350C51E9B%7D.png?ref_type=heads)

![](https://gitlab.com/aiit-ai-lab/ailab-2022/human-resource-machine-group/human-resource-machine/-/raw/main/program_puppet/%E6%BC%94%E7%A4%BA/%7B19552AC9-D957-4401-8C69-60C3DB55AE6F%7D.png?ref_type=heads)

### 游戏页面

![](https://gitlab.com/aiit-ai-lab/ailab-2022/human-resource-machine-group/human-resource-machine/-/raw/main/program_puppet/%E6%BC%94%E7%A4%BA/%7BAE121B96-594B-4f5a-AE89-9BB656EE286D%7D.png?ref_type=heads)

### 游戏执行

![](https://gitlab.com/aiit-ai-lab/ailab-2022/human-resource-machine-group/human-resource-machine/-/raw/main/program_puppet/%E6%BC%94%E7%A4%BA/%7B12EF5649-2111-42b2-9A08-6A315B46C994%7D.png?ref_type=heads)

## 

